# Desafio 02 - Interfaces, Navegação e Armazenamento local

Projeto destinado na resolução do **Desafio 02 - Interfaces, Navegação e Armazenamento local: Daily Diet**.

![Desafio elaborado pela Rocketseat](/docs/images/challengue_cover.png)

Conforme ilustrado na imagem acima, trata-se de um controle da **sua** dieta, que disponibilizar as
seguintes funcionalidades:

- Adicionar uma nova refeição
- Editar uma refeição
- Remover uma refeição da listagem
- Mostrar as estatísticas de progresso na dieta
- Navegação entre telas (priorizada a _em pilha_)
- Armazenamento local das refeições

E entende-se por `refeição` o conjunto das seguintes informações:

- Nome, de até 30 caracteres
- Descrição, de até 280 caracteres
- Data e hora
- Análise de aderência à dieta estipulada

Tanto a interface quanto a experiência do usuário foram definidas pelas Rocketseat e disponibilizadas via projeto Figma.
Servirá de base para o desenvolvimento deste projeto, almejando a implementação de tudo que lá foi estipulado, salvo
limitações do próprio desenvolvedor.

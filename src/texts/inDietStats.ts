export const DIET_STATS = {
    TITLE: 'Estatísticas gerais',
    SUMMARY: 'das refeições dentro da dieta',
    IN_ROLL: 'melhor sequência de pratos dentro da dieta',
    TOTAL_CNT: 'refeições registradas',
    IN_DIET: 'refeições dentro da dieta',
    OUT_DIET: 'refeições fora da dieta',
};

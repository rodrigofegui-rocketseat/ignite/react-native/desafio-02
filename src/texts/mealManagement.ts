export const MEAL_MANAGEMENT = {
    TITLE: 'Refeições',
    ADD: 'Nova refeição',
    UPDATE: 'Editar refeição',
    DETAILS: 'Refeição',
    DELETE: 'Excluir refeição',
    EMPTY: 'Não há refeições conhecidas ainda',
    WARNINGS: {
        DELETE: 'Deseja realmente excluir o registro da refeição?',
    },
    PROCEED: {
        ADD: 'Cadastrar refeição',
        UPDATE: 'Salvar alterações',
        DELETE: 'Sim, excluir',
    },
    ASK: {
        NAME: 'Nome',
        DESCRIPTION: 'Descrição',
        DATE: 'Data',
        TIME: 'Hora',
        IN_DIET: 'Está dentro da dieta?',
    },
    BADGE: {
        IN_DIET: 'dentro da dieta',
        OUT_DIET: 'fora da dieta',
    },
    FEEDBACK: {
        IN_DIET: {
            TITLE: 'Continue assim!',
            DESCRIPTION: {
                BEFORE: 'Você continua ',
                HIGHTLIGHT: 'dentro da dieta',
                AFTER: '. Muito bem!',
            },
        },
        OUT_DIET: {
            TITLE: 'Que pena!',
            DESCRIPTION: {
                BEFORE: 'Você ',
                HIGHTLIGHT: 'saiu da dieta',
                AFTER: ' dessa vez, mas continue se esforçando e não desista!',
            },
        },
        MISSING_FIELDS: {
            TITTLE: 'Que pena!',
            DESCRIPTION:
                'Você acabou não preenchendo todas as informações sobre a refeição, mas pode voltar e completar!',
        },
    },
    LABEL: {
        DETAILS: {
            REGISTERED_AT: 'Data e hora',
        },
    },
};

export const FORMAT = {
    COMPACT_DATE: 'DD.MM.YY',
    LOCALE_DATE: 'DD/MM/YYYY',
    TIME: 'HH:mm',
};

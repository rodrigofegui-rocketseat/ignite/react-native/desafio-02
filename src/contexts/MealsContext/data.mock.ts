import { MealsContextProps } from './types';

export const initialData: MealsContextProps = {
    meals: [],
    stats: {
        adherencePercent: 0,
        inRoll: 0,
        total: 0,
        inDiet: 0,
        outDiet: 0,
        inDietOnZeroAdherence: undefined,
    },
    addMeal: () => ({}),
    getMeal: () => undefined,
    deleteMeal: () => ({}),
    updateMeal: () => ({}),
};

import { DietStats, Meal, MealID, Meals } from '@app/types';

export type MealsContextProps = {
    meals: Meals;
    stats: DietStats;
    addMeal: (meal: Meal) => void;
    getMeal: (targetMeal?: MealID) => Meal | undefined;
    deleteMeal: (targetMeal: MealID) => void;
    updateMeal: (meal: Meal) => void;
};

import { Dispatch, SetStateAction } from 'react';
import uuid from 'react-native-uuid';

import { MealExistsError, MealNonExistsError } from '@app/errors';
import { Meal, MealID, Meals } from '@app/types';

import { alreadyExist, alreadyExistByID } from './meals';
import { syncChanges } from './persist';

export const doAddMeal = (
    newMeal: Meal,
    meals: Meals,
    setMeals: Dispatch<SetStateAction<Meals>>,
) => {
    if (alreadyExist(meals, newMeal)) {
        throw new MealExistsError();
    }

    newMeal.id = uuid.v4() as MealID;

    const newMeals = meals ? [...meals, newMeal] : [newMeal];

    console.debug('A ser incluída', JSON.stringify(newMeal));

    syncChanges(newMeals, setMeals);
};

export const doGetMeal = (meals: Meals, targetMeal?: MealID) =>
    targetMeal ? meals.find((cMeal) => cMeal.id === targetMeal) : undefined;

export const doDeleteMeal = (
    targetMeal: MealID,
    meals: Meals,
    setMeals: Dispatch<SetStateAction<Meals>>,
) => {
    if (!doGetMeal(meals, targetMeal)) {
        throw new MealNonExistsError();
    }

    const newMeals = meals.filter((cMeal) => cMeal.id !== targetMeal);

    syncChanges(newMeals, setMeals);
};

export const doUpdateMeal = (
    updatedMeal: Meal,
    meals: Meals,
    setMeals: Dispatch<SetStateAction<Meals>>,
) => {
    if (!alreadyExistByID(meals, updatedMeal)) {
        throw new MealNonExistsError();
    }

    const newMeals = meals.map((cMeal) =>
        cMeal.id === updatedMeal.id ? updatedMeal : cMeal,
    );

    console.debug('A ser atualizada', JSON.stringify(updatedMeal));

    syncChanges(newMeals, setMeals);
};

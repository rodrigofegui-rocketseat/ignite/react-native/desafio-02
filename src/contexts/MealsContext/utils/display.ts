import { Meals } from '@app/types';

export const sortMeals = (rawMeals: Meals) =>
    rawMeals.sort((meal1, meal2) =>
        meal2.registeredAt.diff(meal1.registeredAt),
    );

import { DietStats, Meals } from '@app/types';

export const getStats = (meals: Meals): DietStats => {
    const total = meals.length;
    let inDiet = 0,
        outDiet = 0,
        inRoll = 0;

    meals.forEach((meal) => {
        if (meal.inDiet) {
            inDiet += 1;
            inRoll += 1;
        } else {
            outDiet += 1;
            inRoll = 0;
        }
    });

    const adherencePercent = total ? 100 * (inDiet / total) : 0;

    return {
        total,
        adherencePercent,
        inRoll,
        inDiet,
        outDiet,
        inDietOnZeroAdherence:
            total > 0 && adherencePercent === 0 ? false : undefined,
    };
};

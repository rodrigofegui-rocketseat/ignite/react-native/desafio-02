import { Dispatch, SetStateAction } from 'react';

import { persistMeals } from '@app/storage/meals';
import { Meals } from '@app/types';

export const syncChanges = (
    meals: Meals,
    setMeals: Dispatch<SetStateAction<Meals>>,
) => {
    persistMeals(meals);

    setMeals(meals);
};

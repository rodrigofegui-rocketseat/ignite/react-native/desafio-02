import { Meal, Meals } from '@app/types';

const MINUTES_TO_CONSIDER_DIFF_MEALS = 5;

export const alreadyExist = (meals: Meals, meal: Meal): boolean => {
    return !!meals.find(
        (cMeal) =>
            cMeal.name === meal.name &&
            cMeal.inDiet === meal.inDiet &&
            isApartEnough(cMeal, meal),
    );
};

export const alreadyExistByID = (meals: Meals, meal: Meal): boolean => {
    return !!meals.find((cMeal) => cMeal.id === meal.id);
};

const isApartEnough = (meal1: Meal, meal2: Meal): boolean =>
    meal1.registeredAt.diff(meal2.registeredAt, 'minutes') <
    MINUTES_TO_CONSIDER_DIFF_MEALS;

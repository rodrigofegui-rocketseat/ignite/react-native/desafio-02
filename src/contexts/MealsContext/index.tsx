import {
    FC,
    PropsWithChildren,
    createContext,
    useEffect,
    useState,
} from 'react';

import { getAllMeals } from '@app/storage/meals';
import { Meal, MealID, Meals } from '@app/types';

import { initialData } from './data.mock';
import { MealsContextProps } from './types';
import {
    doAddMeal,
    doDeleteMeal,
    doGetMeal,
    doUpdateMeal,
} from './utils/actions';
import { sortMeals } from './utils/display';
import { getStats } from './utils/stats';

export const MealsContext = createContext<MealsContextProps>(initialData);

export const MealsContextProvider: FC<PropsWithChildren> = ({ children }) => {
    const [rawMeals, setRawMeals] = useState<Meals>([]);

    const meals = sortMeals(rawMeals);
    const stats = getStats(meals);

    const addMeal = (meal: Meal) => doAddMeal(meal, rawMeals, setRawMeals);
    const getMeal = (targetMeal?: MealID) => doGetMeal(rawMeals, targetMeal);
    const deleteMeal = (targetMeal: MealID) =>
        doDeleteMeal(targetMeal, rawMeals, setRawMeals);
    const updateMeal = (meal: Meal) =>
        doUpdateMeal(meal, rawMeals, setRawMeals);

    useEffect(() => {
        getAllMeals()?.then(setRawMeals);
    }, []);

    return (
        <MealsContext.Provider
            value={{
                meals,
                stats,
                addMeal,
                getMeal,
                deleteMeal,
                updateMeal,
            }}>
            {children}
        </MealsContext.Provider>
    );
};

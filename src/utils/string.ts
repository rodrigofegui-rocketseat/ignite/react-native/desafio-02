export const truncate = (original: string, size: number) => {
    if (original.length <= size) return original;

    const cropped = original.substring(0, size - 3).trim();

    return `${cropped}...`;
};

export const trim = (original?: string): string | undefined => {
    if (!original) {
        return undefined;
    }

    const trimmedText = original.trim();

    return trimmedText.length > 0 ? trimmedText : undefined;
};

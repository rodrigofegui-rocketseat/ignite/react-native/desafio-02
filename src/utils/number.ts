export const fmtPercent = (
    rawPercent: number,
    precision: number = 2,
): string => {
    const txtPercent = rawPercent.toFixed(precision).replace('.', ',');

    return `${txtPercent}%`;
};

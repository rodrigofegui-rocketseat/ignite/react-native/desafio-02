import { AppError } from '@app/errors';
import { ERROR } from '@app/texts';

export const handleError = <T>({
    underGuard,
    onErrorMessage,
}: {
    underGuard: () => T;
    onErrorMessage?: (message: string) => void;
}): T | undefined => {
    try {
        return underGuard();
    } catch (error) {
        if (error instanceof AppError) {
            onErrorMessage && onErrorMessage(error.message);
        } else {
            onErrorMessage && onErrorMessage(ERROR.MESSAGE);

            console.error(error);
        }
    }
};

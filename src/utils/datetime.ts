import { FORMAT } from '@app/texts';
import { NullableMoment } from '@app/types';

export const fmtDate = (targetDatetime: NullableMoment): string =>
    fmt(FORMAT.COMPACT_DATE, targetDatetime);

export const fmtTime = (targetDatetime: NullableMoment): string =>
    fmt(FORMAT.TIME, targetDatetime);

export const fmtLocaleDate = (targetDatetime: NullableMoment): string =>
    fmt(FORMAT.LOCALE_DATE, targetDatetime);

export const fmtLocaleDatetime = (targetDatetime?: NullableMoment): string =>
    targetDatetime
        ? `${fmtLocaleDate(targetDatetime)} às ${fmtTime(targetDatetime)}`
        : '';

const fmt = (targetFormat: string, targetDatetime: NullableMoment): string =>
    targetDatetime ? targetDatetime.format(targetFormat) : '';

import { Meal, MealByDate, Meals, MealsByDate } from '@app/types';

import { fmtDate } from './datetime';

export const groupByDate = (meals: Meals): MealsByDate => {
    const groupedByDate = [] as MealsByDate;

    meals.forEach((meal) => {
        const date = extractDate(meal);

        const foundedGroup = findDateGroup(groupedByDate, date);

        if (foundedGroup) {
            addMeal(foundedGroup, meal);
        } else {
            createGroup(groupedByDate, date, meal);
        }
    });

    return groupedByDate;
};

const addMeal = (group: MealByDate, meal: Meal) => {
    group.data.push(meal);
};

const createGroup = (groups: MealsByDate, date: string, meal: Meal) => {
    groups.push({
        title: date,
        data: [meal],
    });
};

const extractDate = (meal: Meal): string => {
    return fmtDate(meal.registeredAt);
};

const findDateGroup = (
    groups: MealsByDate,
    date: string,
): MealByDate | undefined => {
    return groups.find((group) => group.title === date);
};

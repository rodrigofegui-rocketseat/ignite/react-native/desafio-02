import { Children, FC, ReactElement, ReactNode } from 'react';

type MapFunction = (rawItem: ReactNode, index: number) => ReactElement;

export const getCntChildren = (children: ReactNode): number =>
    Children.count(children);

export const getChildOfType = (children: ReactNode, targetType: FC<any>) => {
    return Children.toArray(children).find(
        (child) =>
            _isReactElement(child) &&
            child.type?.toString() === targetType.toString(),
    ) as ReactElement | undefined;
};

export const getFstChild = (children: ReactNode) => {
    return Children.toArray(children).at(0);
};

export const mapChildren = (children: ReactNode, mapFunc: MapFunction) => {
    return Children.toArray(children).map(mapFunc);
};

const _isReactElement = (targetElement: any): targetElement is ReactElement => {
    return targetElement.hasOwnProperty('type');
};

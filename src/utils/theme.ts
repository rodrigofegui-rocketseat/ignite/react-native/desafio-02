import { MainTheme } from '@app/themes';
import { ThemedColorKeysType } from '@app/types';

export const isThemedColor = (
    targetColor: string,
): targetColor is ThemedColorKeysType => {
    return targetColor in MainTheme.COLORS;
};

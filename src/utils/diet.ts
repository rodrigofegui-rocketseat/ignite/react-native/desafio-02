import { ContainerColorStyleProps } from '@app/components/DDHeader/Root/types';

export const getTheme = (
    cPercentage?: number,
    inDiet?: boolean,
): ContainerColorStyleProps => {
    if (cPercentage) {
        return getThemeByPercentage(cPercentage);
    }

    return getThemeByBool(inDiet);
};

const getThemeByPercentage = (cPercentage: number): ContainerColorStyleProps =>
    getThemeByBool(inAdherence(cPercentage, 50));

const getThemeByBool = (inDiet?: boolean): ContainerColorStyleProps => {
    if (inDiet === undefined) {
        return 'LIGHT';
    }

    return inDiet ? 'PRIMARY' : 'SECONDARY';
};

const inAdherence = (cPercentage: number, threshold: number) =>
    cPercentage >= threshold;

import { NavigationContainer } from '@react-navigation/native';
import { FC } from 'react';

import { DDBaseView } from '@app/components';

import { AppRoutes } from './app';

export const Routes: FC = () => {
    return (
        <DDBaseView withPadding={false}>
            <NavigationContainer>
                <AppRoutes />
            </NavigationContainer>
        </DDBaseView>
    );
};

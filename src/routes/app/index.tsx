import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { FC } from 'react';

import { MealsContextProvider } from '@app/contexts/MealsContext';

import { screenOptions } from './config';
import { screens } from './screens';

const { Navigator, Screen } = createNativeStackNavigator();

export const AppRoutes: FC = () => {
    const screensComponents = screens.map((cScreen) => {
        return (
            <Screen
                key={cScreen.name}
                {...cScreen}
            />
        );
    });

    return (
        <MealsContextProvider>
            <Navigator screenOptions={screenOptions}>
                {screensComponents}
            </Navigator>
        </MealsContextProvider>
    );
};

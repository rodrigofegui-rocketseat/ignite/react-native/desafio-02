import { Details } from '@app/screens/Details';
import { FeedbackCompleteManipulation } from '@app/screens/FeedbackCompleteManipulation';
import { LandingPage } from '@app/screens/LandingPage';
import { Manipulation } from '@app/screens/Manipulation';
import { Stats } from '@app/screens/Stats';
import { ScreenProps } from '@app/types';

export const screens: ScreenProps[] = [
    {
        name: 'LandingPage',
        component: LandingPage,
    },
    {
        name: 'Stats',
        component: Stats,
    },
    {
        name: 'Manipulation',
        component: Manipulation,
    },
    {
        name: 'Details',
        component: Details,
    },
    {
        name: 'FeedbackCompleteManipulation',
        component: FeedbackCompleteManipulation,
    },
];

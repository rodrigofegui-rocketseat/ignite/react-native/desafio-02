import { AppError } from './commom';

export class MealExistsError extends AppError {
    constructor() {
        super('Refeição já existe!');
    }
}

export class MealNonExistsError extends AppError {
    constructor() {
        super('Refeição não existe nos registros!');
    }
}

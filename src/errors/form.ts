import { MEAL_MANAGEMENT } from '@app/texts';

import { AppError } from './commom';

export class MissingFieldsError extends AppError {
    constructor() {
        super(MEAL_MANAGEMENT.FEEDBACK.MISSING_FIELDS.DESCRIPTION);
    }
}

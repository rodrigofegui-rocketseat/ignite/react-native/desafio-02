export const COMMON = {
    COLORS: {
        WHITE: '#FFFFFF',

        GRAY_100: '#FAFAFA',
        GRAY_200: '#EFF0F0',
        GRAY_300: '#DDDEDF',
        GRAY_400: '#B9BBBC',
        GRAY_500: '#5C6265',
        GRAY_600: '#333638',
        GRAY_700: '#1B1D1E',

        GREEN_500: '#639339',
        GREEN_300: '#CBE4B4',
        GREEN_200: '#E5F0DB',

        RED_500: '#BF3B44',
        RED_300: '#F3BABD',
        RED_200: '#F4E6E7',
    },
    FONT: {
        LINE_HEIGHT: 1.3,
        FAMILY: {
            REGULAR: 'NunitoSans_400Regular',
            BOLD: 'NunitoSans_700Bold',
        },
        SIZE: {
            XS: 12,
            SM: 14,
            MD: 16,
            LG: 18,
            XL: 24,
            XL2: 32,
        },
    },
    SPACING: {
        XS2: 4,
        XS: 8,
        SM: 16,
        MD: 24,
        LG: 32,
        XL: 40,
        XL2: 50,
    },
};

const APP_PREFIX = '@daily-diet';

export const COLLECTIONS = {
    MEALS: `${APP_PREFIX}:meals`,
};

import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';

import { Meals } from '@app/types';
import { handleError } from '@app/utils/errors';

import { COLLECTIONS } from './config';

export const persistMeals = (meals: Meals) =>
    handleError({
        underGuard: async () => {
            const storage = JSON.stringify(meals);

            await AsyncStorage.setItem(COLLECTIONS.MEALS, storage);
        },
    });

export const getAllMeals = () =>
    handleError({
        underGuard: async () => {
            const storage = await AsyncStorage.getItem(COLLECTIONS.MEALS);

            if (!storage) {
                return [];
            }

            const rawMeals: Meals = JSON.parse(storage);

            return rawMeals.map((meal) => ({
                ...meal,
                registeredAt: moment(meal.registeredAt),
            }));
        },
    });

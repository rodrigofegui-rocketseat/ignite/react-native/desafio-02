import { MainTheme } from '@app/themes';

export type ThemeType = typeof MainTheme;

export type ThemedColorType = typeof MainTheme.COLORS;
export type ThemedColorKeysType = keyof ThemedColorType;

export type ThemedSpacingType = typeof MainTheme.SPACING;
export type ThemedSpacingKeysType = keyof ThemedSpacingType;

export type ThemedFontSizeType = typeof MainTheme.FONT.SIZE;
export type ThemedFontSizeKeysType = keyof ThemedFontSizeType;

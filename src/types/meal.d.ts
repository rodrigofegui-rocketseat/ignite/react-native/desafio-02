import { Moment } from 'moment';

export const MAX_MEAL_NAME_SIZE = 30;

export const MAX_MEAL_DESCRIPTION_SIZE = 280;

export type MealID = string;

export type Meal = {
    id: MealID;
    registeredAt: Moment;
    name: string;
    description: string;
    inDiet: boolean;
};

export type Meals = Meal[];

export type MealByDate = {
    title: string;
    data: Meals;
};

export type MealsByDate = MealByDate[];

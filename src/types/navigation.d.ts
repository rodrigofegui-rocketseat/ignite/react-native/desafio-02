import { ParamListBase } from '@react-navigation/native';

import { MealID } from './meal';

export declare global {
    namespace ReactNavigation {
        interface RootParamList extends ParamListBase {
            LandingPage: undefined;
            Stats: undefined;
            Manipulation?: {
                targetMeal: MealID;
            };
            Details: {
                targetMeal: MealID;
            };
            FeedbackCompleteManipulation: {
                inDiet: boolean;
            };
        }
    }
}

import * as Phosphor from 'phosphor-react-native';

export type IconName = keyof typeof Phosphor;

export type DietStats = {
    adherencePercent: number;
    inRoll: number;
    total: number;
    inDiet: number;
    outDiet: number;
    inDietOnZeroAdherence?: boolean;
};

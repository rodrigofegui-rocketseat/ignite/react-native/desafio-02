import { RouteProp } from '@react-navigation/native';
import { FC } from 'react';

export type Routename = keyof ReactNavigation.RootParamList;

export type ScreenProps = {
    name: Routename;
    component: FC;
};

export type UseAppRouteProp<Routename> = RouteProp<
    ReactNavigation.RootParamList,
    Routename
>;

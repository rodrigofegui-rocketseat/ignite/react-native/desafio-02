import { Moment } from 'moment';

export type NullableMoment = Moment | null;

import { FC, PropsWithChildren } from 'react';

import { DDText } from '../../DDText';

export const Subtitle: FC<PropsWithChildren> = ({ children }) => {
    return <DDText size='MD'>{children}</DDText>;
};

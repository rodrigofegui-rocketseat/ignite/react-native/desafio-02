import { Root } from './Root';
import { Subtitle } from './Subtitle';
import { Title } from './Title';

export const DDHeader = {
    Root,
    Title,
    Subtitle,
};

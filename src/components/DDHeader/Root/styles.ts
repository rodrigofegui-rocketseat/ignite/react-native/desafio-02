import { StatusBar } from 'react-native';
import styled, { css } from 'styled-components/native';

import { StyleContainerProps, StyleCustomStatusBarProps } from './types';
import { getBgColor } from './utils/theme';

export const Container = styled.View<StyleContainerProps>`
    align-items: center;
    justify-content: center;

    margin-bottom: ${({ theme }) => -1 * theme.SPACING.MD}px;

    height: ${({ hasSubtitle }) => (hasSubtitle ? 170 : 100)}px;

    padding-bottom: ${({ theme, hasSubtitle }) =>
        hasSubtitle ? theme.SPACING.MD : theme.SPACING.XS}px;

    ${({ theme, color }) => {
        const bgColor = getBgColor(color);

        return css`
            background-color: ${theme.COLORS[bgColor]};
        `;
    }}
`;

export const CustomStatusBar = styled(
    StatusBar,
).attrs<StyleCustomStatusBarProps>(({ theme, color }) => {
    const bgColor = getBgColor(color);

    return {
        barStyle: 'dark-content',
        backgroundColor: theme.COLORS[bgColor],
        translucent: false,
    };
})``;

import { ThemedColorType } from '@app/types';

export type RootProps = {
    color?: ContainerColorStyleProps;
    backTo?: keyof ReactNavigation.RootParamList;
};

export type ContainerColorStyleProps =
    | keyof Pick<ThemedColorType, 'PRIMARY' | 'SECONDARY'>
    | 'LIGHT';

export type StyleContainerProps = {
    color: ContainerColorStyleProps;
    hasSubtitle: boolean;
};

export type StyleCustomStatusBarProps = {
    color: ContainerColorStyleProps;
};

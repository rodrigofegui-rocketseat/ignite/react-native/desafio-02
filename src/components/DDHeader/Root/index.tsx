import { FC, PropsWithChildren } from 'react';

import { getChildOfType } from '@app/utils/children';

import { BackTo } from '../BackTo';
import { Subtitle } from '../Subtitle';
import { Title } from '../Title';

import { Container, CustomStatusBar } from './styles';
import { RootProps } from './types';
import { getTitleComponent } from './utils/components';
import { getBackToColor } from './utils/theme';

export const Root: FC<PropsWithChildren<RootProps>> = ({
    children,
    backTo,
    color = 'PRIMARY',
}) => {
    const titleChild = getChildOfType(children, Title);
    const subtitleChild = getChildOfType(children, Subtitle);

    return (
        <>
            <CustomStatusBar color={color} />

            <Container
                hasSubtitle={!!subtitleChild}
                color={color}>
                <BackTo
                    color={getBackToColor(color)}
                    backTo={backTo}
                />

                {titleChild && getTitleComponent(titleChild, subtitleChild)}

                {subtitleChild}
            </Container>
        </>
    );
};

import { ThemedColorKeysType } from '@app/types';
import { isThemedColor } from '@app/utils/theme';

import { ContainerColorStyleProps } from '../types';

export const getBackToColor = (
    rawColor: ContainerColorStyleProps,
): ThemedColorKeysType => {
    const targetColor = rawColor.toUpperCase();

    return isThemedColor(targetColor) ? targetColor : 'GRAY_600';
};

export const getBgColor = (
    rawColor: ContainerColorStyleProps,
): ThemedColorKeysType => {
    const targetColor = rawColor.toUpperCase();
    const targetLightColor = `${targetColor}_LIGHT`;

    return isThemedColor(targetLightColor) ? targetLightColor : 'GRAY_300';
};

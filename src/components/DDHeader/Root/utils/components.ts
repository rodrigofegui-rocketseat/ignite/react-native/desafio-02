import { ReactElement, cloneElement } from 'react';

export const getTitleComponent = (
    rawTitle: ReactElement,
    rawSubtitle?: ReactElement,
): ReactElement => {
    const targetSize = rawSubtitle ? 'XL2' : 'LG';

    return cloneElement(rawTitle, { size: targetSize });
};

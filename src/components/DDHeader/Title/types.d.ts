import { DDTextProps } from '../../DDText/types';

export type TitleProps = Pick<DDTextProps, 'size'>;

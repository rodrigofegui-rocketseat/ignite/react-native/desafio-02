import { FC, PropsWithChildren } from 'react';

import { DDText } from '../../DDText';

import { TitleProps } from './types';

export const Title: FC<PropsWithChildren<TitleProps>> = ({
    children,
    size = 'XL2',
}) => {
    return (
        <DDText
            bold
            size={size}>
            {children}
        </DDText>
    );
};

import { ArrowLeft } from 'phosphor-react-native';
import styled, { css } from 'styled-components/native';

import { StyleIconProps } from './types';

export const Clicable = styled.TouchableOpacity`
    ${({ theme }) => css`
        padding-right: ${theme.SPACING.SM}px;
        padding-top: ${theme.SPACING.XS}px;
        padding-bottom: ${theme.SPACING.XS}px;
    `}
`;

export const Container = styled.View`
    position: absolute;

    ${({ theme }) => css`
        top: ${theme.SPACING.MD}px;
        left: ${theme.SPACING.SM}px;
    `}
`;

export const Icon = styled(ArrowLeft).attrs<StyleIconProps>(
    ({ theme, color }) => ({
        size: theme.SPACING.MD,
        color: theme.COLORS[color],
        weight: 'bold',
    }),
)``;

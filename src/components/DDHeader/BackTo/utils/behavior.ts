import { NavigationProp } from '@react-navigation/native';

export const navigationControl = (
    navigation: NavigationProp<ReactNavigation.RootParamList>,
    backTo?: keyof ReactNavigation.RootParamList,
) => {
    if (!backTo) {
        return navigation.goBack();
    }

    return navigation.navigate(backTo);
};

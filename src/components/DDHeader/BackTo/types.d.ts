import { ThemedColorKeysType } from '@app/types';

export type BackToProps = {
    color: ThemedColorKeysType;
    backTo?: keyof ReactNavigation.RootParamList;
};

export type StyleIconProps = {
    color: ThemedColorKeysType;
};

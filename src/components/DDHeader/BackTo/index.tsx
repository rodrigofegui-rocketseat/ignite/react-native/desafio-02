import { useNavigation } from '@react-navigation/native';
import { FC } from 'react';

import { Clicable, Container, Icon } from './styles';
import { BackToProps } from './types';
import { navigationControl } from './utils/behavior';

export const BackTo: FC<BackToProps> = ({ backTo, color }) => {
    const navigation = useNavigation();

    return (
        <Container>
            <Clicable onPress={() => navigationControl(navigation, backTo)}>
                <Icon color={color} />
            </Clicable>
        </Container>
    );
};

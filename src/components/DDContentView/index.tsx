import styled, { css } from 'styled-components/native';

export const DDContentView = styled.View`
    flex: 1;

    ${({ theme }) => css`
        background-color: ${theme.COLORS.GRAY_100};

        padding-left: ${theme.SPACING.SM}px;
        padding-right: ${theme.SPACING.SM}px;
        padding-top: ${theme.SPACING.LG}px;

        border-top-right-radius: ${theme.SPACING.MD}px;
        border-top-left-radius: ${theme.SPACING.MD}px;
    `}
`;

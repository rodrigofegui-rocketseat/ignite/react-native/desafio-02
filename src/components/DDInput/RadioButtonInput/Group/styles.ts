import styled from 'styled-components/native';

import { DDFlex } from '@app/components/DDFlex';

export const Container = styled(DDFlex.Row).attrs(() => ({
    justifyContent: 'space-between',
}))``;

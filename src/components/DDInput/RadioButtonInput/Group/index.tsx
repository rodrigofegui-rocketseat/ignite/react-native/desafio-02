import { PropsWithChildren, ReactElement, forwardRef, useState } from 'react';

import { getFstChild, mapChildren } from '@app/utils/children';

import { ItemProps } from '../Item/types';

import { Container } from './styles';
import { GroupProps } from './types';
import { mapperItemComponent } from './utils/components';

export const Group = forwardRef<any, PropsWithChildren<GroupProps>>(
    ({ children, initialValue }, ref) => {
        const fstChild = getFstChild(children) as ReactElement<ItemProps>;

        const [selectedValue, setSelectedValue] =
            useState<typeof fstChild.props.value>(initialValue);

        ref.current = selectedValue;

        const interceptedChildren = mapChildren(
            children,
            mapperItemComponent(selectedValue, setSelectedValue),
        );

        return <Container>{interceptedChildren}</Container>;
    },
);

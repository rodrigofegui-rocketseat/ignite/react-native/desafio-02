import { Dispatch, ReactElement, ReactNode, cloneElement } from 'react';

import { ItemProps } from '../../Item/types';

export const mapperItemComponent = (
    selected: any,
    setSelected: Dispatch<any>,
) => {
    return (rawItem: ReactNode, index: number): ReactElement => {
        const typedRaw = rawItem as ReactElement<ItemProps>;
        const onPress = (value: any) => setSelected(value);

        return cloneElement(typedRaw, {
            onPress,
            key: index,
            isActive: selected === typedRaw.props.value,
        });
    };
};

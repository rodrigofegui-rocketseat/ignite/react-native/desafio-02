import { ThemedColorKeysType } from '@app/types';

import { ContentProps, IdentifierProps } from '../types';

export const getBgColor = ({
    isActive,
    identifier,
}: ContentProps): ThemedColorKeysType => {
    if (!isActive) {
        return 'GRAY_200';
    }

    if (identifier === 'primary') {
        return 'PRIMARY_LIGHT';
    }

    return 'SECONDARY_LIGHT';
};

export const getBorderColor = ({
    identifier,
}: IdentifierProps): ThemedColorKeysType => {
    if (identifier === 'primary') {
        return 'PRIMARY';
    }

    return 'SECONDARY';
};

import styled, { css } from 'styled-components/native';

import { DDFlex } from '@app/components/DDFlex';
import { DDInDietIndicator } from '@app/components/DDInDietIndicator';
import { DDText } from '@app/components/DDText';

import { ContentProps, IdentifierProps } from './types';
import { getBgColor, getBorderColor } from './utils/theme';

export const Container = styled(DDFlex.Column).attrs(() => ({
    size: 6,
}))`
    align-items: center;
    justify-content: center;
`;

export const Content = styled.TouchableOpacity<ContentProps>`
    align-items: center;
    justify-content: center;

    flex-direction: row;

    width: 100%;

    ${({ theme, isActive, identifier }) => css`
        background-color: ${theme.COLORS[getBgColor({ isActive, identifier })]};

        border-radius: ${theme.SPACING.XS}px;
        border-width: ${isActive ? 1 : 0}px;
        border-color: ${theme.COLORS[getBorderColor({ identifier })]};

        padding-top: ${theme.SPACING.SM}px;
        padding-bottom: ${theme.SPACING.SM}px;
    `};
`;

export const Identifier = styled(DDInDietIndicator).attrs<IdentifierProps>(
    ({ identifier }) => ({
        size: 'XS',
        inDiet: identifier === 'primary',
    }),
)`
    margin-right: ${({ theme }) => theme.SPACING.XS}px;
`;

export const Text = styled(DDText).attrs(() => ({
    size: 'SM',
    bold: true,
}))``;

export type IdentifierProps = {
    identifier: 'primary' | 'secondary';
};

export type ItemProps = IdentifierProps & {
    content: string;
    value: any;
    onPress?: (value: any) => void;
    isActive?: boolean;
};

export type ContentProps = Pick<ItemProps, 'identifier' | 'isActive'>;

import { FC } from 'react';

import { Content, Container, Identifier, Text } from './styles';
import { ItemProps } from './types';

export const Item: FC<ItemProps> = ({
    content,
    identifier,
    value,
    onPress,
    isActive,
}) => {
    const handleClick = () => onPress && onPress(value);

    return (
        <Container>
            <Content
                isActive={isActive}
                identifier={identifier}
                onPress={handleClick}>
                <Identifier identifier={identifier} />
                <Text>{content}</Text>
            </Content>
        </Container>
    );
};

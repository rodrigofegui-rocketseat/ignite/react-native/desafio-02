import { Group } from './Group';
import { Item } from './Item';

export const RadioButtonInput = {
    Group,
    Item,
};

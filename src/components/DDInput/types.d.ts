export type InitialValueProps<T> = {
    initialValue?: T;
};

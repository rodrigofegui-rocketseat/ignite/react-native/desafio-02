import styled, { css } from 'styled-components/native';

export const SingleLineInput = styled.TextInput.attrs(({ theme }) => ({
    autoCapitalize: 'none',
    autoComplete: 'off',
    autoCorrect: false,
    blurOnSubmit: true,
    cursorColor: theme.COLORS.GRAY_700,
    multiline: false,
    returnKeyType: 'done',
    spellCheck: false,
    textAlignVertical: 'center',
}))`
    ${({ theme }) => css`
        font-family: ${theme.FONT.FAMILY.REGULAR};
        color: ${theme.COLORS.GRAY_700}

        border-width: 1px;
        border-color: ${theme.COLORS.GRAY_300};
        border-radius: ${theme.SPACING.XS}px;

        padding-left: ${theme.SPACING.SM}px;
        padding-right: ${theme.SPACING.SM}px;
        padding-top: ${theme.SPACING.XS}px;
        padding-bottom: ${theme.SPACING.XS}px;
    `}
`;

export const MultipleLinesInput = styled(SingleLineInput).attrs(() => ({
    multiline: true,
    textAlignVertical: 'top',
}))`
    ${({ theme }) => css`
        padding-top: ${theme.SPACING.SM}px;
        padding-bottom: ${theme.SPACING.SM}px;
    `}
`;

import { Dispatch, ForwardedRef, SetStateAction } from 'react';
import { Keyboard, TextInput } from 'react-native';

import { ThemedColorType } from '@app/types';
import { trim } from '@app/utils/string';

export const handleKeyboardDismiss = (ref: ForwardedRef<TextInput>) => {
    const handlerKeyboardOnHide = Keyboard.addListener(
        'keyboardDidHide',
        () => {
            ref?.current?.blur();
        },
    );

    return () => {
        handlerKeyboardOnHide.remove();
    };
};

export const handleFocusInOut = (
    ref: ForwardedRef<TextInput>,
    colors: ThemedColorType,
) => {
    if (!ref) return;

    ref!.current?.setNativeProps({
        style: {
            borderColor: ref!.current?.isFocused()
                ? colors.GRAY_700
                : colors.GRAY_300,
        },
    });
};

export const handleChangeText = (
    ref: ForwardedRef<TextInput>,
    cText?: string,
    setValue?: Dispatch<SetStateAction<string | undefined>>,
) => {
    if (!ref?.current) return;

    ref!.current!.value = cText;

    if (setValue) {
        setValue(cText);
    }
};

export const handleSubmittedText = (
    ref: ForwardedRef<TextInput>,
    setValue: Dispatch<SetStateAction<string | undefined>>,
) => {
    const trimmedText = trim(ref!.current!.value);

    ref!.current!.value = trimmedText;

    setValue(trimmedText);
};

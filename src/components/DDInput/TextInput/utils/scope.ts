import { MultipleLinesInput, SingleLineInput } from '../styles';

export const MIN_LINE_CNT = 1;

export const getInputComponent = (numberOfLines: number) => {
    if (isSingleLine(numberOfLines)) return SingleLineInput;

    return MultipleLinesInput;
};

export const minLineGuard = (numberOfLines: number): number =>
    Math.max(MIN_LINE_CNT, numberOfLines);

const isSingleLine = (numberOfLines: number): boolean =>
    numberOfLines <= MIN_LINE_CNT;

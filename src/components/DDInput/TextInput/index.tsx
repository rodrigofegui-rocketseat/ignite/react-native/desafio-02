import { forwardRef, useEffect, useState } from 'react';
import {
    NativeSyntheticEvent,
    TextInput as NativeTextInput,
    TextInputSubmitEditingEventData,
} from 'react-native';
import { useTheme } from 'styled-components/native';

import { DDTextInputProps } from './types';
import {
    handleChangeText,
    handleFocusInOut,
    handleKeyboardDismiss,
    handleSubmittedText,
} from './utils/behavior';
import { MIN_LINE_CNT, getInputComponent, minLineGuard } from './utils/scope';

export const TextInput = forwardRef<NativeTextInput, DDTextInputProps>(
    (
        {
            onSubmitEditing,
            maxLength,
            numberOfLines = MIN_LINE_CNT,
            initialValue,
        },
        textInputRef,
    ) => {
        const { COLORS } = useTheme();
        const [value, setValue] = useState(initialValue);

        numberOfLines = minLineGuard(numberOfLines);

        const InputComponent = getInputComponent(numberOfLines);

        const handleSubmitEditing = (
            event: NativeSyntheticEvent<TextInputSubmitEditingEventData>,
        ) => {
            handleSubmittedText(textInputRef, setValue);
            onSubmitEditing && onSubmitEditing(event);
        };

        useEffect(() => {
            handleChangeText(textInputRef, initialValue);
        }, []);

        useEffect(() => {
            handleKeyboardDismiss(textInputRef);
        }, []);

        return (
            <InputComponent
                ref={textInputRef}
                onSubmitEditing={handleSubmitEditing}
                onEndEditing={handleSubmitEditing}
                onFocus={() => handleFocusInOut(textInputRef, COLORS)}
                onBlur={() => handleFocusInOut(textInputRef, COLORS)}
                onChangeText={(text: string) =>
                    handleChangeText(textInputRef, text, setValue)
                }
                value={value}
                numberOfLines={numberOfLines}
                rows={numberOfLines}
                maxLength={maxLength}
            />
        );
    },
);

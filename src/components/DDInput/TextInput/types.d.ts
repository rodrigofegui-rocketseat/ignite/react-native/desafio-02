import { TextInputProps } from 'react-native';

export type DDTextInputProps = Pick<
    TextInputProps,
    'onSubmitEditing' | 'maxLength' | 'numberOfLines'
> &
    InitialValueProps<string>;

import { DateTimeInput } from './DateTimeInput';
import { Label } from './Label';
import { RadioButtonInput } from './RadioButtonInput';
import { Root } from './Root';
import { TextInput } from './TextInput';

export const DDInput = {
    Root,
    Label,
    TextInput,
    DateTimeInput,
    RadioButtonInput,
};

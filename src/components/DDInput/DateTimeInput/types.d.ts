import { Moment } from 'moment';
import { TextInputProps } from 'react-native';

import { InitialValueProps } from '../types';

export type Mode = 'date' | 'time';

export type InputModeProps = { mode: Mode };

export type DDDateTimeInputProps = InputMode &
    Pick<TextInputProps, 'onSubmitEditing'> &
    InitialValueProps<Moment>;

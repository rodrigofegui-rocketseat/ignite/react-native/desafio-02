import { DateTimePickerEvent } from '@react-native-community/datetimepicker';
import { forwardRef, useEffect, useState } from 'react';
import { TextInput as NativeTextInput } from 'react-native';

import { Container, FeedbackSelected, Input } from './styles';
import { DDDateTimeInputProps } from './types';
import { handleChangeDatetime, handleError } from './utils/behavior';
import { getDatetimeValue } from './utils/render';

export const DateTimeInput = forwardRef<NativeTextInput, DDDateTimeInputProps>(
    ({ mode, onSubmitEditing, initialValue }, textInputRef) => {
        const [open, setOpen] = useState<boolean | undefined>(undefined);

        const hangleChange = ({
            nativeEvent: { timestamp },
        }: DateTimePickerEvent) => {
            handleChangeDatetime(textInputRef, mode, timestamp);

            setOpen(false);

            onSubmitEditing && onSubmitEditing();
        };

        useEffect(() => {
            handleChangeDatetime(textInputRef, mode, initialValue);

            setOpen(false);
        }, []);

        return (
            <Container onPress={() => setOpen(true)}>
                <FeedbackSelected
                    ref={textInputRef}
                    onFocus={() => setOpen(true)}
                    onBlur={() => setOpen(false)}
                    value={textInputRef?.current?.value}
                />

                {open && (
                    <Input
                        mode={mode}
                        value={getDatetimeValue(
                            mode,
                            textInputRef?.current?.value,
                        )}
                        onChange={hangleChange}
                        onError={(error) => handleError(error, setOpen)}
                    />
                )}
            </Container>
        );
    },
);

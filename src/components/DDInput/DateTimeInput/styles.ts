import DateTimePicker from '@react-native-community/datetimepicker';
import styled, { css } from 'styled-components/native';

import { OPTIONS } from '@app/texts';

import { InputModeProps } from './types';

export const Container = styled.TouchableOpacity`
    min-height: 45.8px;

    justify-content: center;
`;

export const FeedbackSelected = styled.TextInput.attrs(({ theme }) => ({
    cursorColor: theme.COLORS.GRAY_700,
    spellCheck: false,
    multiline: false,
    readOnly: true,
    showSoftInputOnFocus: false,
    textAlignVertical: 'center',
    autoCapitalize: 'none',
    autoComplete: 'off',
}))`
    width: 100%;

    ${({ theme }) => css`
        font-family: ${theme.FONT.FAMILY.REGULAR};
        color: ${theme.COLORS.GRAY_700}

        border-width: 1px;
        border-color: ${theme.COLORS.GRAY_300};
        border-radius: ${theme.SPACING.XS}px;

        padding-left: ${theme.SPACING.SM}px;
        padding-right: ${theme.SPACING.SM}px;
        padding-top: ${theme.SPACING.XS}px;
        padding-bottom: ${theme.SPACING.XS}px;
    `}
`;

export const Input = styled(DateTimePicker).attrs<InputModeProps>(
    ({ mode, theme }) => ({
        mode,
        display: 'spinner',
        textColor: theme.COLORS.GRAY_700,
        positiveButton: {
            label: OPTIONS.CONFIRM,
            textColor: theme.COLORS.PRIMARY,
        },
        negativeButton: {
            label: OPTIONS.CANCEL,
            textColor: theme.COLORS.GRAY_500,
        },
        maximumDate: new Date(),
        is24Hour: mode === 'time',
    }),
)``;

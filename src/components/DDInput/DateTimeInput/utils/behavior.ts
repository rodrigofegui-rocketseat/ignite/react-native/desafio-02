import moment, { Moment, isMoment } from 'moment';
import { Dispatch, ForwardedRef, SetStateAction } from 'react';
import { TextInput } from 'react-native';

import { handleChangeText } from '../../TextInput/utils/behavior';
import { Mode } from '../types';

import { fmtDisplay } from './render';

export const handleChangeDatetime = (
    ref: ForwardedRef<TextInput>,
    mode: Mode,
    cValue?: number | Moment,
) => {
    if (!cValue) return;

    const targetDatetime = isMoment(cValue) ? cValue : moment(cValue);

    handleChangeText(ref, fmtDisplay(mode, targetDatetime));
};

export const handleError = (
    error: any,
    setOpen: Dispatch<SetStateAction<boolean | undefined>>,
) => {
    console.debug('[ERROR] DateTimeInput');
    console.debug(error);
    setOpen(false);
};

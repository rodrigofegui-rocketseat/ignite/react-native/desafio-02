import moment, { Moment } from 'moment';

import { FORMAT } from '@app/texts';
import { NullableMoment } from '@app/types';
import { fmtLocaleDate, fmtTime } from '@app/utils/datetime';

import { Mode } from '../types';

export const getDatetimeValue = (mode: Mode, cValue?: string): Date => {
    let cDatetime: Moment;

    if (!cValue) {
        cDatetime = moment();
    } else if (mode === 'date') {
        cDatetime = moment(`${cValue} -0300`, `${FORMAT.LOCALE_DATE} Z`);
    } else {
        cDatetime = moment()
            .hours(Number(cValue.split(':')[0]))
            .minutes(Number(cValue.split(':')[1]));
    }

    return cDatetime.toDate();
};

export const fmtDisplay = (mode: Mode, cValue?: NullableMoment) => {
    if (!cValue) {
        return '';
    }

    return mode === 'date' ? fmtLocaleDate(cValue) : fmtTime(cValue);
};

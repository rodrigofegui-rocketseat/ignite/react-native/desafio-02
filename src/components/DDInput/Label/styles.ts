import styled from 'styled-components/native';

import { DDText } from '../../DDText';

export const Text = styled(DDText).attrs(() => ({
    bold: true,
    size: 'SM',
}))`
    margin-bottom: ${({ theme }) => theme.SPACING.XS2}px;
`;

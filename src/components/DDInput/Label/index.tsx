import { FC, PropsWithChildren } from 'react';

import { Text } from './styles';

export const Label: FC<PropsWithChildren> = ({ children }) => (
    <Text>{children}</Text>
);

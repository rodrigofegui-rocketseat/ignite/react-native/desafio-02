import { FC, PropsWithChildren } from 'react';

import { Label } from '@app/components/DDButton/Label';
import { getChildOfType } from '@app/utils/children';

import { DateTimeInput } from '../DateTimeInput';
import { RadioButtonInput } from '../RadioButtonInput';
import { TextInput } from '../TextInput';

import { Container } from './styles';

export const Root: FC<PropsWithChildren> = ({ children }) => {
    const labelChild = getChildOfType(children, Label);
    const textChild = getChildOfType(children, TextInput);
    const datetimeChild = getChildOfType(children, DateTimeInput);
    const radioChild = getChildOfType(children, RadioButtonInput.Group);

    return (
        <Container>
            {labelChild}
            {textChild || datetimeChild || radioChild}
        </Container>
    );
};

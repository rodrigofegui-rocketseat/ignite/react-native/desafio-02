import styled, { css } from 'styled-components/native';

import { StyleContainerProps } from './types';
import { getBgColor } from './utils/theme';

export const DDInDietIndicator = styled.View<StyleContainerProps>`
    ${({ theme, inDiet = true, size = 'SM' }) => css`
        width: ${theme.SPACING[size]}px;
        height: ${theme.SPACING[size]}px;

        border-radius: ${theme.SPACING[size] / 2}px;

        background-color: ${theme.COLORS[getBgColor(inDiet, size)]};
    `}
`;

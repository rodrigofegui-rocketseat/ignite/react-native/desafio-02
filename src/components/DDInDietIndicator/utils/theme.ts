import { ThemedColorKeysType } from '@app/types';

import { DDInDietIndicatorSize } from '../types';

export const getBgColor = (
    inDiet: boolean,
    size: DDInDietIndicatorSize,
): ThemedColorKeysType => {
    const baseColor: ThemedColorKeysType = inDiet ? 'PRIMARY' : 'SECONDARY';
    const variant = size === 'SM' ? '_MUTED' : '';

    return `${baseColor}${variant}`;
};

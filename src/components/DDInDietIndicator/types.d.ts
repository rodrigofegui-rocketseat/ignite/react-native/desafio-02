import { ThemedSpacingType } from '@app/types';

export type DDInDietIndicatorSize = keyof Pick<ThemedSpacingType, 'XS' | 'SM'>;

export type DDInDietIndicatorProps = {
    inDiet?: boolean;
    size?: DDInDietIndicatorSize;
};

export type StyleContainerProps = {
    inDiet: boolean;
    size: DDInDietIndicatorSize;
};

import styled, { css } from 'styled-components/native';

import { DDTextProps } from './types';

export const DDText = styled.Text<DDTextProps>`
    ${({ theme, size = 'MD', bold = false, color = 'GRAY_700' }) => css`
        font-family: ${bold
            ? theme.FONT.FAMILY.BOLD
            : theme.FONT.FAMILY.REGULAR};

        font-size: ${theme.FONT.SIZE[size]}px;
        line-height: ${theme.FONT.SIZE[size] * theme.FONT.LINE_HEIGHT}px;

        color: ${theme.COLORS[color]};
    `};
`;

import { ThemedColorKeysType, ThemedFontSizeKeysType } from '@app/types';

export type DDTextProps = {
    size?: ThemedFontSizeKeysType;
    color?: ThemedColorKeysType;
    bold?: boolean;
};

import { Column } from './Column';
import { Row } from './Row';

export const DDFlex = {
    Row,
    Column,
};

import { JustifyContent, ThemedSpacingKeysType } from '@app/types';

export type RowProps = {
    marginBottom?: ThemedSpacingKeysType;
    justifyContent?: JustifyContent;
};

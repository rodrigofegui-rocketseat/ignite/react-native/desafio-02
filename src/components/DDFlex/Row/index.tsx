import styled from 'styled-components/native';

import { RowProps } from './types';

export const Row = styled.View<RowProps>`
    width: 100%;
    flex-direction: row;
    justify-content: ${({ justifyContent = 'initial' }) => justifyContent};

    margin-bottom: ${({ theme, marginBottom }) =>
        marginBottom ? theme.SPACING[marginBottom] : 0}px;
`;

// TODO: Try to implement the Range type from @app/types/types
export type ColumnSize = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;

export type ColumnProps = {
    size: ColumnSize;
};

import { ColumnSize } from './types';

export const getStep = (size: ColumnSize): number => {
    if (size === 12) return 8.34;

    if (size === 6) return 7.9;

    return 8;
};

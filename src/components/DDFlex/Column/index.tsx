import styled from 'styled-components/native';

import { ColumnProps } from './types';
import { getStep } from './utils';

export const Column = styled.View<ColumnProps>`
    width: ${({ size = 12 }) => getStep(size) * size}%;
`;

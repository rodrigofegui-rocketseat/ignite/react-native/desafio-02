import * as Phosphor from 'phosphor-react-native';
import { FC } from 'react';
import { useTheme } from 'styled-components/native';

import { ThemedColorKeysType } from '@app/types';

import { Container } from './styles';
import { DDIconProps } from './types';

export const Icon: FC<DDIconProps> = ({ name, size, color }) => {
    const theme = useTheme();

    const TargetIcon = Phosphor[name] as FC<Phosphor.IconProps>;

    const finalSize = size || theme.SPACING.SM;
    const finalColor: ThemedColorKeysType = color || 'GRAY_700';

    return (
        <Container>
            <TargetIcon
                weight='bold'
                size={finalSize}
                color={theme.COLORS[finalColor]}
            />
        </Container>
    );
};

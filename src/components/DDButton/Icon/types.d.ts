import { ThemedColorKeysType } from '@app/types';

export type DDIconProps = {
    name: IconName;
    size?: number;
    color?: ThemedColorKeysType;
};

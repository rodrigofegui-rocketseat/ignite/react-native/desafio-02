import styled from 'styled-components/native';

export const Container = styled.View`
    margin-right: ${({ theme }) => theme.SPACING.XS}px;
`;

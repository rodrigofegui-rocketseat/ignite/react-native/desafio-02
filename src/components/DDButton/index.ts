import { Icon } from './Icon';
import { Label } from './Label';
import { Root } from './Root';

export const DDButton = {
    Icon,
    Label,
    Root,
};

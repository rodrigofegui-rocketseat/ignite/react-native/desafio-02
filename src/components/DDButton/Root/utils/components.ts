import { ReactElement, cloneElement } from 'react';

import { COLORS } from './constants';
import { getTargetColorSet } from './theme';

export const getIconChild = (rawIconChild: ReactElement, isFilled: boolean) =>
    getChild(rawIconChild, isFilled, 'icon');

export const getLabelChild = (rawLabelChild: ReactElement, isFilled: boolean) =>
    getChild(rawLabelChild, isFilled, 'label');

const getChild = (rawChild: ReactElement, isFilled: boolean, key: string) => {
    if (!rawChild) return rawChild;

    const targetColorSet = getTargetColorSet(isFilled);

    return cloneElement(rawChild, {
        key,
        color: COLORS[targetColorSet].FOREGROUND,
    });
};

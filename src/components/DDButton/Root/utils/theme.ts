import { RootColorContext } from '../types';

export const getTargetColorSet = (isFilled: boolean): RootColorContext =>
    isFilled ? 'FILLED' : 'UNFILLED';

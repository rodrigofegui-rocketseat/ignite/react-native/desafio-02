import { RootColors } from '../types';

export const COLORS: RootColors = {
    FILLED: {
        BACKGROUND: 'GRAY_600',
        FOREGROUND: 'WHITE',
        ACTIVE_BACKGROUND: 'GRAY_700',
    },
    UNFILLED: {
        BACKGROUND: 'GRAY_100',
        FOREGROUND: 'GRAY_700',
        ACTIVE_BACKGROUND: 'GRAY_300',
    },
};

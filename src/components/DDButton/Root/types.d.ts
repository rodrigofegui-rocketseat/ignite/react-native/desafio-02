import { TouchableHighlightProps } from 'react-native';

import { ThemedColorKeysType } from '@app/types';

export type RootColorContext = 'FILLED' | 'UNFILLED';
type ColorContext = 'BACKGROUND' | 'FOREGROUND' | 'ACTIVE_BACKGROUND';

export type RootColors = Record<
    RootColorContext,
    Record<ColorContext, ThemedColorKeysType>
>;

export type RootProps = Pick<TouchableHighlightProps, 'onPress'> & {
    isFilled?: boolean;
    width?: number;
};

export type StyleContainerProps = {
    isFilled: boolean;
    width: number;
};

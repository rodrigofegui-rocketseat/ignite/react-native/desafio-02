import { FC, PropsWithChildren } from 'react';

import { getChildOfType } from '@app/utils/children';

import { Icon } from '../Icon';
import { Label } from '../Label';

import { Container, Content } from './styles';
import { RootProps } from './types';
import { getIconChild, getLabelChild } from './utils/components';

export const Root: FC<PropsWithChildren<RootProps>> = ({
    children,
    onPress,
    isFilled = true,
    width = 100,
}) => {
    const iconChild = getChildOfType(children, Icon);
    const labelChild = getChildOfType(children, Label);

    return (
        <Container
            isFilled={isFilled}
            width={width}
            onPress={onPress}>
            <Content>
                {iconChild && getIconChild(iconChild, isFilled)}
                {labelChild && getLabelChild(labelChild, isFilled)}
            </Content>
        </Container>
    );
};

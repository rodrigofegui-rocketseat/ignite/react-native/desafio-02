import styled, { css } from 'styled-components/native';

import { StyleContainerProps } from './types';
import { COLORS } from './utils/constants';
import { getTargetColorSet } from './utils/theme';

export const Container = styled.TouchableHighlight.attrs<StyleContainerProps>(
    ({ theme, isFilled }) => {
        const targetColorSet = getTargetColorSet(isFilled);

        return {
            underlayColor:
                theme.COLORS[COLORS[targetColorSet].ACTIVE_BACKGROUND],
        };
    },
)<StyleContainerProps>`
    width: ${({ width }) => `${width}%`};

    ${({ theme, isFilled }) => {
        const targetColorSet = getTargetColorSet(isFilled);

        return css`
            border-radius: ${theme.SPACING.XS}px;
            border-width: 1px;
            border-color: ${theme.COLORS.GRAY_700};
            min-height: ${theme.SPACING.XL2}px;
            background-color: ${theme.COLORS[
                COLORS[targetColorSet].BACKGROUND
            ]};
        `;
    }};
`;

export const Content = styled.View`
    flex: 1;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;

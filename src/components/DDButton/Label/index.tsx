import { FC, PropsWithChildren } from 'react';

import { CustomText } from './styles';
import { LabelProps } from './types';

export const Label: FC<PropsWithChildren<LabelProps>> = ({
    children,
    ...rest
}) => <CustomText {...rest}>{children}</CustomText>;

import { ThemedColorKeysType } from '@app/types';

export type LabelProps = {
    color?: ThemedColorKeysType;
};

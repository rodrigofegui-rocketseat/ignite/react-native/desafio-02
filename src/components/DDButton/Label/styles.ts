import styled from 'styled-components/native';

import { DDText } from '../../DDText';

export const CustomText = styled(DDText).attrs(() => ({
    bold: true,
    size: 'MD',
}))``;

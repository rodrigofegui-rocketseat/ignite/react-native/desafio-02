import styled, { css } from 'styled-components/native';

import { DDText } from '../../DDText';

export const Title = styled(DDText).attrs(() => ({
    bold: true,
    size: 'LG',
}))`
    text-align: center;

    ${({ theme }) => css`
        padding-left: ${theme.SPACING.MD}px;
        padding-right: ${theme.SPACING.MD}px;

        margin-bottom: ${theme.SPACING.LG}px;
    `}
`;

import { FC, PropsWithChildren } from 'react';

export const Body: FC<PropsWithChildren> = ({ children }) => {
    return <>{children}</>;
};

import { FC, PropsWithChildren } from 'react';

export const Footer: FC<PropsWithChildren> = ({ children }) => {
    return <>{children}</>;
};

import styled, { css } from 'styled-components/native';

export const FakeInput = styled.TextInput.attrs(() => ({
    showSoftInputOnFocus: false,
}))`
    height: 0px;
`;

export const Modal = styled.Modal.attrs(() => ({
    animationType: 'fade',
    transparent: true,
}))`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const Container = styled.View`
    flex: 1;
    justify-content: center;
    justify-content: center;
    background-color: rgba(0, 0, 0, 0.5);
`;

export const Content = styled.View`
    align-items: center;

    ${({ theme }) => css`
        background-color: ${theme.COLORS.GRAY_100};

        border-radius: ${theme.SPACING.SM}px;

        margin-left: ${theme.SPACING.SM}px;
        margin-right: ${theme.SPACING.SM}px;

        padding: ${theme.SPACING.MD}px;
    `}
`;

import { PropsWithChildren, forwardRef, useState } from 'react';
import { TextInput } from 'react-native';

import { getChildOfType } from '@app/utils/children';

import { Body } from '../Body';
import { Footer } from '../Footer';
import { Title } from '../Title';

import { Container, Content, FakeInput, Modal } from './styles';

export const Root = forwardRef<TextInput, PropsWithChildren>(
    ({ children }, ref) => {
        const [isVisible, setIsVisible] = useState(false);

        const titleChild = getChildOfType(children, Title);
        const bodyChild = getChildOfType(children, Body);
        const footerChild = getChildOfType(children, Footer);

        return (
            <>
                <FakeInput
                    ref={ref}
                    onFocus={() => setIsVisible(true)}
                    onBlur={() => setIsVisible(false)}
                />
                <Modal visible={isVisible}>
                    <Container>
                        <Content>
                            {titleChild}
                            {bodyChild}
                            {footerChild}
                        </Content>
                    </Container>
                </Modal>
            </>
        );
    },
);

import { Body } from './Body';
import { Footer } from './Footer';
import { Root } from './Root';
import { Title } from './Title';

export const DDModal = {
    Body,
    Footer,
    Root,
    Title,
};

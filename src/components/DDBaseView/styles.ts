import { SafeAreaView } from 'react-native-safe-area-context';
import styled, { css } from 'styled-components/native';

import { StyleContainerProps } from './types';

export const Container = styled(SafeAreaView)<StyleContainerProps>`
    flex: 1;

    ${({ theme, withPadding, insets }) => css`
        background-color: ${theme.COLORS.GRAY_100};

        padding-top: ${withPadding ? theme.SPACING.MD + insets.top : 0}px;
        padding-left: ${withPadding ? theme.SPACING.SM + insets.left : 0}px;
        padding-right: ${withPadding ? theme.SPACING.SM + insets.right : 0}px;
    `};
`;

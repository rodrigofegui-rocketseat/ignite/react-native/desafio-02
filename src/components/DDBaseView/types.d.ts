import { EdgeInsets } from 'react-native-safe-area-context';

export type StyleContainerProps = {
    withPadding: boolean;
    insets: EdgeInsets;
};

export type DDBaseViewProps = {
    withPadding?: boolean;
};

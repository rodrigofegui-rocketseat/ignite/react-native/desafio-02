import { FC, PropsWithChildren } from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { Container } from './styles';
import { DDBaseViewProps } from './types';

export const DDBaseView: FC<PropsWithChildren<DDBaseViewProps>> = ({
    children,
    withPadding = true,
}) => {
    const insets = useSafeAreaInsets();

    return (
        <Container
            withPadding={withPadding}
            insets={insets}>
            {children}
        </Container>
    );
};

import { Description } from './Description';
import { Navigator } from './Navigator';
import { Root } from './Root';
import { Title } from './Title';

export const DDInfoCard = {
    Description,
    Navigator,
    Root,
    Title,
};

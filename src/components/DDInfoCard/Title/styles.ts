import styled from 'styled-components/native';

import { DDText } from '../../DDText';

export const Text = styled(DDText).attrs(() => ({
    bold: true,
    size: 'XL2',
}))`
    padding-bottom: ${({ theme }) => theme.SPACING.XS}px;
`;

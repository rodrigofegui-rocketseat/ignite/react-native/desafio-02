import { FC, PropsWithChildren } from 'react';

import { Text } from './styles';

export const Title: FC<PropsWithChildren> = ({ children }) => {
    return <Text>{children}</Text>;
};

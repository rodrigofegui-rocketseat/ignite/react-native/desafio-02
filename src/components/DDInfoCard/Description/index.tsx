import { FC, PropsWithChildren } from 'react';

import { Text } from './styles';

export const Description: FC<PropsWithChildren> = ({ children }) => {
    return <Text>{children}</Text>;
};

import styled from 'styled-components/native';

import { DDText } from '../../DDText';

export const Text = styled(DDText).attrs(() => ({
    size: 'SM',
}))`
    text-align: center;
`;

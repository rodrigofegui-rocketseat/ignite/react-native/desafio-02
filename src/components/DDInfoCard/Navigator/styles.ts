import { ArrowUpRight } from 'phosphor-react-native';
import styled, { css } from 'styled-components/native';

import { StylesIconProps } from './types';

export const Container = styled.View`
    position: absolute;
    top: 0;
    right: 0;

    ${({ theme }) => css`
        border-radius: ${theme.SPACING.XS}px;
        padding: ${theme.SPACING.XS}px;
    `}
`;

export const Icon = styled(ArrowUpRight).attrs<StylesIconProps>(
    ({ theme, color }) => ({
        size: theme.SPACING.LG,
        color: theme.COLORS[color],
    }),
)``;

import { ThemedColorKeysType } from '@app/types';

export type NavigatorProps = {
    to: () => void;
    color?: ThemedColorKeysType;
};

export type StylesIconProps = {
    color: ThemedColorKeysType;
};

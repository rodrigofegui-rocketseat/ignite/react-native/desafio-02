import { FC } from 'react';

import { Container, Icon } from './styles';
import { NavigatorProps } from './types';

export const Navigator: FC<NavigatorProps> = ({ color = 'PRIMARY' }) => {
    return (
        <Container>
            <Icon color={color} />
        </Container>
    );
};

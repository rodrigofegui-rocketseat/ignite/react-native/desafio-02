import { ThemedColorType } from '@app/types';

export type RootProps = {
    color?: StyleContainerColorStyleProps;
    widthPercentage?: number;
};

export type StyleContainerColorStyleProps =
    | keyof Pick<ThemedColorType, 'PRIMARY' | 'SECONDARY'>
    | 'LIGHT';

export type StyleContainerProps = {
    color: StyleContainerColorStyleProps;
    width: number;
};

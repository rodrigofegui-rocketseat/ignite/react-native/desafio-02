import { ThemedColorKeysType } from '@app/types';
import { isThemedColor } from '@app/utils/theme';

import { StyleContainerColorStyleProps } from '../types';

export const getNavigatorColor = (
    rawColor: StyleContainerColorStyleProps,
): ThemedColorKeysType => {
    const targetColor = rawColor.toUpperCase();

    return isThemedColor(targetColor) ? targetColor : 'GRAY_600';
};

export const getBgColor = (
    rawColor: StyleContainerColorStyleProps,
): ThemedColorKeysType => {
    const targetColor = rawColor.toUpperCase();
    const targetLightColor = `${targetColor}_LIGHT`;

    return isThemedColor(targetLightColor) ? targetLightColor : 'GRAY_200';
};

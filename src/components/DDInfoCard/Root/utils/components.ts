import {
    ExoticComponent,
    Fragment,
    PropsWithChildren,
    ReactElement,
    cloneElement,
} from 'react';
import { TouchableOpacity } from 'react-native';
import { StyledComponent } from 'styled-components';
import { DefaultTheme } from 'styled-components/native';

import { Clicable } from '../styles';
import { StyleContainerColorStyleProps } from '../types';

import { getNavigatorColor } from './theme';

export const getNavigatorChild = (
    rawNavigator: ReactElement,
    color: StyleContainerColorStyleProps,
) => cloneElement(rawNavigator, { color: getNavigatorColor(color) });

export const getWrapper = (
    rawNavigator?: ReactElement,
): [
    (
        | StyledComponent<typeof TouchableOpacity, DefaultTheme, object, never>
        | ExoticComponent<PropsWithChildren<any>>
    ),
    object,
] => {
    const WrapperComponent = rawNavigator ? Clicable : Fragment;
    const wrapperProps = rawNavigator
        ? { onPress: rawNavigator?.props.to }
        : {};

    return [WrapperComponent, wrapperProps];
};

import { FC, PropsWithChildren } from 'react';

import { getChildOfType } from '@app/utils/children';

import { Description } from '../Description';
import { Navigator } from '../Navigator';
import { Title } from '../Title';

import { Container } from './styles';
import { RootProps } from './types';
import { getNavigatorChild, getWrapper } from './utils/components';

export const Root: FC<PropsWithChildren<RootProps>> = ({
    children,
    color = 'LIGHT',
    widthPercentage = 100,
}) => {
    const titleChild = getChildOfType(children, Title);
    const descriptionChild = getChildOfType(children, Description);
    const navigatorChild = getChildOfType(children, Navigator);

    const [WrapperComponent, wrapperProps] = getWrapper(navigatorChild);

    return (
        <WrapperComponent {...wrapperProps}>
            <Container
                color={color}
                width={widthPercentage}>
                {navigatorChild && getNavigatorChild(navigatorChild, color)}
                {titleChild}
                {descriptionChild}
            </Container>
        </WrapperComponent>
    );
};

import styled, { css } from 'styled-components/native';

import { StyleContainerProps } from './types';
import { getBgColor } from './utils/theme';

export const Clicable = styled.TouchableOpacity``;

export const Container = styled.View<StyleContainerProps>`
    width: ${({ width }) => `${width}%`};
    align-items: center;
    justify-content: center;

    ${({ theme, color }) => {
        const bgColor = getBgColor(color);

        return css`
            padding-left: ${theme.SPACING.SM}px;
            padding-right: ${theme.SPACING.SM}px;
            padding-top: ${theme.SPACING.MD}px;
            padding-bottom: ${theme.SPACING.MD}px;

            border-radius: ${theme.SPACING.XS}px;

            background-color: ${theme.COLORS[bgColor]};
        `;
    }}
`;

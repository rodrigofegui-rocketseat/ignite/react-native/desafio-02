import { DDLogo } from './DDLogo';
import { DDUser } from './DDUser';
import { Container } from './styles';

export const Header = () => {
    return (
        <Container>
            <DDLogo />
            <DDUser />
        </Container>
    );
};

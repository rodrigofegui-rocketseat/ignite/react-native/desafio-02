import styled from 'styled-components/native';

const imageWidth = 40;

export const Image = styled.Image`
    aspect-ratio: 1; /* original width / original height */
    width: ${imageWidth}px;

    border-radius: ${imageWidth / 2}px;
    border-color: ${({ theme }) => theme.COLORS.GRAY_700};
    border-width: 2px;
`;

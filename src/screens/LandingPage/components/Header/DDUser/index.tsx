import placeholderUser from '@assets/images/placehold_user.webp';

import { Image } from './styles';

export const DDUser = () => {
    return <Image source={placeholderUser} />;
};

import styled from 'styled-components/native';

export const Logo = styled.Image`
    aspect-ratio: 2.21; /* original width / original height */
    width: 82px;
`;

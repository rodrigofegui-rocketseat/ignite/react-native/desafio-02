import logoImg from '@assets/icon.png';
import { FC } from 'react';

import { Logo } from './styles';

export const DDLogo: FC = () => {
    return <Logo source={logoImg} />;
};

import { useNavigation } from '@react-navigation/native';
import { FC, useContext } from 'react';

import { DDInfoCard } from '@app/components';
import { MealsContext } from '@app/contexts/MealsContext';
import { DIET_STATS } from '@app/texts';
import { getTheme } from '@app/utils/diet';
import { fmtPercent } from '@app/utils/number';

import { Container } from './styles';

export const InDietAdherence: FC = () => {
    const navigation = useNavigation();
    const {
        stats: { adherencePercent, inDietOnZeroAdherence },
    } = useContext(MealsContext);

    return (
        <Container>
            <DDInfoCard.Root
                color={getTheme(adherencePercent, inDietOnZeroAdherence)}>
                <DDInfoCard.Navigator to={() => navigation.navigate('Stats')} />

                <DDInfoCard.Title>
                    {fmtPercent(adherencePercent)}
                </DDInfoCard.Title>

                <DDInfoCard.Description>
                    {DIET_STATS.SUMMARY}
                </DDInfoCard.Description>
            </DDInfoCard.Root>
        </Container>
    );
};

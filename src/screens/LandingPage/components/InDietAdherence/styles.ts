import styled from 'styled-components/native';

export const Container = styled.View`
    margin-top: ${({ theme }) => theme.SPACING.LG}px;
`;

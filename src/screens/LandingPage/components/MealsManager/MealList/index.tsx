import { FC, useContext } from 'react';

import { MealsContext } from '@app/contexts/MealsContext';
import { groupByDate } from '@app/utils/meals';

import { EmptyList } from './EmptyList';
import { MealItem } from './MealItem';
import { SectionHeader } from './SectionHeader';
import { Container, Gradient, List } from './styles';

export const MealList: FC = () => {
    const { meals } = useContext(MealsContext);

    const groupedMeals = groupByDate(meals);

    return (
        <Container>
            <Gradient />

            <List
                sections={groupedMeals}
                keyExtractor={(item) => item.id}
                renderSectionHeader={({ section }) => (
                    <SectionHeader title={section.title} />
                )}
                renderItem={({ item }) => <MealItem meal={item} />}
                ListEmptyComponent={EmptyList}
            />
        </Container>
    );
};

import styled, { css } from 'styled-components/native';

import { DDText } from '@app/components';

const BORDER_SIZE = 1;

export const Container = styled.TouchableOpacity`
    flex-direction: row;

    align-items: center;

    border-width: ${BORDER_SIZE}px;

    ${({ theme }) => css`
        margin-top: ${theme.SPACING.XS}px;

        border-color: ${theme.COLORS.GRAY_300};
        border-radius: ${theme.SPACING.XS}px;

        padding: ${theme.SPACING.SM}px;
    `}
`;

export const MealTime = styled(DDText).attrs(() => ({
    bold: true,
    size: 'SM',
}))``;

export const MealNameWrapper = styled.View`
    flex: 1;
`;

export const MealName = styled(DDText).attrs(() => ({
    bold: false,
    size: 'MD',
}))``;

export const Separator = styled.View`
    height: 100%;

    border-width: ${BORDER_SIZE * 1}px;

    ${({ theme }) => css`
        border-color: ${theme.COLORS.GRAY_300};

        margin-left: ${theme.SPACING.XS}px;
        margin-right: ${theme.SPACING.XS}px;
    `}
`;

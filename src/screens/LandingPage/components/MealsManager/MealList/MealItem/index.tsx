import { useNavigation } from '@react-navigation/native';
import { FC } from 'react';

import { DDInDietIndicator } from '@app/components';
import { fmtTime } from '@app/utils/datetime';
import { truncate } from '@app/utils/string';

import {
    Container,
    MealName,
    MealNameWrapper,
    MealTime,
    Separator,
} from './styles';
import { MealItemProps } from './types';

const TRUNCATE_SIZE = 26;

export const MealItem: FC<MealItemProps> = ({ meal }) => {
    const navigation = useNavigation();

    return (
        <Container
            onPress={() =>
                navigation.navigate('Details', { targetMeal: meal.id })
            }>
            <MealTime>{fmtTime(meal.registeredAt)}</MealTime>

            <Separator />

            <MealNameWrapper>
                <MealName>{truncate(meal.name, TRUNCATE_SIZE)}</MealName>
            </MealNameWrapper>

            <DDInDietIndicator inDiet={meal.inDiet} />
        </Container>
    );
};

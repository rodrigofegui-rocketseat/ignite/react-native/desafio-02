import { Meal } from '@app/types';

export type MealItemProps = {
    meal: Meal;
};

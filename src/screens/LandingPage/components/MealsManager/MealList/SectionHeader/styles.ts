import styled, { css } from 'styled-components/native';

import { DDText } from '@app/components';

export const Container = styled.View`
    ${({ theme }) => css`
        margin-top: ${theme.SPACING.LG}px;
    `}
`;

export const CustomText = styled(DDText).attrs(() => ({
    bold: true,
    size: 'LG',
}))``;

import { FC } from 'react';

import { Container, CustomText } from './styles';
import { SectionHeaderProps } from './types';

export const SectionHeader: FC<SectionHeaderProps> = ({ title }) => {
    return (
        <Container>
            <CustomText>{title}</CustomText>
        </Container>
    );
};

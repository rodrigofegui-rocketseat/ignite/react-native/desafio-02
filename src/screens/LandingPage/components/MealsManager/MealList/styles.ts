import { LinearGradient } from 'expo-linear-gradient';
import { SectionListProps } from 'react-native';
import styled from 'styled-components/native';

import { Meal, MealsByDate } from '@app/types';

const GRADIENT_HEIGHT = 100;

export const Container = styled.View`
    flex: 1;
`;

export const Gradient = styled(LinearGradient).attrs(({ theme }) => ({
    colors: ['transparent', theme.COLORS.GRAY_100],
}))`
    position: sticky;
    top: 80%;

    height: ${GRADIENT_HEIGHT}px;

    z-index: 1000;
`;

export const List = styled.SectionList.attrs<
    SectionListProps<Meal, MealsByDate>
>(({ theme }) => ({
    showsVerticalScrollIndicator: false,
    contentContainerStyle: {
        paddingBottom: 1.5 * theme.SPACING.XL2,
    },
}))`
    flex: 1;

    margin-top: ${({ theme }) =>
        -1 * (theme.SPACING.XS + GRADIENT_HEIGHT) + 10}px;
`;

import styled from 'styled-components/native';

import { DDText } from '@app/components';

export const Description = styled(DDText).attrs(() => ({
    color: 'GRAY_400',
}))`
    text-align: center;
    font-style: italic;

    margin-top: ${({ theme }) => theme.SPACING.LG}px;
`;

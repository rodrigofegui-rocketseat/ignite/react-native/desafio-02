import { FC } from 'react';

import { MEAL_MANAGEMENT } from '@app/texts';

import { Description } from './styles';

export const EmptyList: FC = () => {
    return <Description>{MEAL_MANAGEMENT.EMPTY}</Description>;
};

import { useNavigation } from '@react-navigation/native';
import { FC } from 'react';

import { DDButton } from '@app/components';
import { MEAL_MANAGEMENT } from '@app/texts';

import { MealList } from './MealList';
import { Container, Label } from './styles';

export const MealsManager: FC = () => {
    const navigation = useNavigation();

    return (
        <Container>
            <Label>{MEAL_MANAGEMENT.TITLE}</Label>

            <DDButton.Root onPress={() => navigation.navigate('Manipulation')}>
                <DDButton.Icon name='Plus' />
                <DDButton.Label>{MEAL_MANAGEMENT.ADD}</DDButton.Label>
            </DDButton.Root>

            <MealList />
        </Container>
    );
};

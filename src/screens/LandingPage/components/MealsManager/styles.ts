import styled from 'styled-components/native';

import { DDText } from '@app/components';

export const Container = styled.View`
    flex: 1;
    width: 100%;

    margin-top: ${({ theme }) => theme.SPACING.LG}px;
`;

export const Label = styled(DDText).attrs(() => ({
    size: 'MD',
}))`
    margin-bottom: ${({ theme }) => theme.SPACING.XS}px;
`;

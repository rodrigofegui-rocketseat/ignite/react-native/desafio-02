import { FC } from 'react';

import { Header, InDietAdherence, MealsManager } from './components';
import { Container, CustomStatusBar } from './styles';

export const LandingPage: FC = () => {
    return (
        <Container>
            <CustomStatusBar />

            <Header />

            <InDietAdherence />

            <MealsManager />
        </Container>
    );
};

import { StatusBar } from 'expo-status-bar';
import styled from 'styled-components/native';

import { DDBaseView } from '@app/components/DDBaseView';

export const Container = styled(DDBaseView)``;

export const CustomStatusBar = styled(StatusBar).attrs(({ theme }) => {
    return {
        barStyle: 'light-content',
        backgroundColor: theme.COLORS.GRAY_100,
        translucent: false,
    };
})``;

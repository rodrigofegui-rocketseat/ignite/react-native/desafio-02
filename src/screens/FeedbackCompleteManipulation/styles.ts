import styled, { css } from 'styled-components/native';

import { DDBaseView, DDText } from '@app/components';

import { InDietProps } from './types';

const IMAGE_WIDTH = 250;

export const Container = styled(DDBaseView)``;

export const Content = styled.View`
    flex: 1;

    justify-content: center;
    align-items: center;
`;

export const Title = styled(DDText).attrs(() => ({
    bold: true,
    size: 'XL',
}))<InDietProps>`
    ${({ theme, inDiet }) => css`
        color: ${inDiet ? theme.COLORS.PRIMARY : theme.COLORS.SECONDARY};

        margin-bottom: ${theme.SPACING.XS}px;
    `}
`;

export const Description = styled(DDText)`
    text-align: center;
`;

export const Image = styled.Image.attrs<InDietProps>(({ inDiet }) => ({
    source: inDiet
        ? require('@assets/images/in_diet_Illustration.webp')
        : require('@assets/images/out_diet_Illustration.webp'),
}))`
    /* original height / original width = 1.3 */
    width: ${IMAGE_WIDTH}px;
    height: ${1.3 * IMAGE_WIDTH}px;

    ${({ theme }) => css`
        margin-top: ${theme.SPACING.XL}px;
        margin-bottom: ${theme.SPACING.XL}px;
    `}
`;

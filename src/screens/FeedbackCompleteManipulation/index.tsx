import { useNavigation, useRoute } from '@react-navigation/native';
import { FC } from 'react';

import { DDButton, DDText } from '@app/components';
import { MEAL_MANAGEMENT, NAVIGATION } from '@app/texts';

import { Container, Content, Description, Image, Title } from './styles';

export const FeedbackCompleteManipulation: FC = () => {
    const navigation = useNavigation();
    const {
        params: { inDiet },
    } = useRoute();
    const customContent = inDiet
        ? MEAL_MANAGEMENT.FEEDBACK.IN_DIET
        : MEAL_MANAGEMENT.FEEDBACK.OUT_DIET;

    return (
        <Container>
            <Content>
                <Title inDiet={inDiet}>{customContent.TITLE}</Title>

                <Description>
                    {customContent.DESCRIPTION.BEFORE}
                    <DDText bold>{customContent.DESCRIPTION.HIGHTLIGHT}</DDText>
                    {customContent.DESCRIPTION.AFTER}
                </Description>

                <Image inDiet={inDiet} />

                <DDButton.Root
                    width={70}
                    onPress={() => navigation.navigate('LandingPage')}>
                    <DDButton.Label>{NAVIGATION.BACK_TO_HOME}</DDButton.Label>
                </DDButton.Root>
            </Content>
        </Container>
    );
};

import { FC } from 'react';

import { Actions, FullDescription, Header } from './components';
import { Container, Content } from './styles';

export const Details: FC = () => {
    return (
        <Container>
            <Header />

            <Content>
                <FullDescription />

                <Actions />
            </Content>
        </Container>
    );
};

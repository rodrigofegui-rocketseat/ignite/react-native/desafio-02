import styled from 'styled-components/native';

import { DDBaseView, DDContentView } from '@app/components';

export const Container = styled(DDBaseView).attrs(() => ({
    withPadding: false,
}))``;

export const Content = styled(DDContentView)`
    justify-content: space-between;
`;

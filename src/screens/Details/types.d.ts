import { UseAppRouteProp } from '@app/types';

export type UseDetailsRouteProp = UseAppRouteProp<'Details'>;

import { useNavigation, useRoute } from '@react-navigation/native';
import { FC, useContext } from 'react';
import { Alert } from 'react-native';

import { MealsContext } from '@app/contexts/MealsContext';
import { MEAL_MANAGEMENT } from '@app/texts';
import { fmtLocaleDatetime } from '@app/utils/datetime';

import { UseDetailsRouteProp } from '../../types';

import { Badge } from './components';
import {
    Container,
    Description,
    LabelRegisteredAt,
    Name,
    RegisteredAt,
} from './styles';

export const FullDescription: FC = () => {
    const navigation = useNavigation();
    const {
        params: { targetMeal },
    } = useRoute<UseDetailsRouteProp>();
    const { getMeal } = useContext(MealsContext);

    const meal = getMeal(targetMeal);

    if (!meal) {
        Alert.alert(
            'Que pena!',
            'Não foi possível recuperar as informações da refeição. Tente novamente mais tarde',
        );
        navigation.goBack();

        return null;
    }

    return (
        <Container>
            <Name>{meal?.name}</Name>

            <Description>{meal?.description}</Description>

            <LabelRegisteredAt>
                {MEAL_MANAGEMENT.LABEL.DETAILS.REGISTERED_AT}
            </LabelRegisteredAt>

            <RegisteredAt>{fmtLocaleDatetime(meal?.registeredAt)}</RegisteredAt>

            <Badge inDiet={meal?.inDiet} />
        </Container>
    );
};

import styled, { css } from 'styled-components/native';

import { DDText } from '@app/components';

export const Container = styled.View`
    flex-direction: row;
`;

export const Content = styled.View`
    flex-direction: row;
    align-items: center;

    ${({ theme }) => css`
        background-color: ${theme.COLORS.GRAY_200};

        border-radius: ${theme.SPACING.LG}px;

        padding-top: ${theme.SPACING.XS}px;
        padding-bottom: ${theme.SPACING.XS}px;
        padding-left: ${theme.SPACING.SM}px;
        padding-right: ${theme.SPACING.SM}px;
    `}
`;

export const Label = styled(DDText).attrs(() => ({
    size: 'SM',
}))`
    margin-left: ${({ theme }) => theme.SPACING.XS}px;
`;

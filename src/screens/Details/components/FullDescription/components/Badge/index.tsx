import { FC } from 'react';

import { DDInDietIndicator } from '@app/components';
import { MEAL_MANAGEMENT } from '@app/texts';

import { Container, Content, Label } from './styles';
import { BadgeProps } from './types';

export const Badge: FC<BadgeProps> = ({ inDiet = false }) => {
    const labelText = inDiet
        ? MEAL_MANAGEMENT.BADGE.IN_DIET
        : MEAL_MANAGEMENT.BADGE.OUT_DIET;

    return (
        <Container>
            <Content>
                <DDInDietIndicator
                    inDiet={inDiet}
                    size='XS'
                />

                <Label>{labelText}</Label>
            </Content>
        </Container>
    );
};

import styled from 'styled-components/native';

import { DDText } from '@app/components';

export const Container = styled.View``;

export const Name = styled(DDText).attrs(() => ({
    bold: true,
    size: 'LG',
}))`
    margin-bottom: ${({ theme }) => theme.SPACING.XS}px;
`;

export const Description = styled(DDText)`
    margin-bottom: ${({ theme }) => theme.SPACING.MD}px;
`;

export const LabelRegisteredAt = styled(DDText).attrs(() => ({
    bold: true,
    size: 'SM',
}))`
    margin-bottom: ${({ theme }) => theme.SPACING.XS}px;
`;

export const RegisteredAt = styled(DDText)`
    margin-bottom: ${({ theme }) => theme.SPACING.MD}px;
`;

import { useNavigation } from '@react-navigation/native';
import { forwardRef, useContext } from 'react';
import { TextInput } from 'react-native';

import { DDButton, DDFlex, DDModal } from '@app/components';
import { MealsContext } from '@app/contexts/MealsContext';
import { MEAL_MANAGEMENT, OPTIONS } from '@app/texts';

import { ConfirmDeleteProps } from './types';

export const ConfirmDelete = forwardRef<TextInput, ConfirmDeleteProps>(
    ({ targetMeal }, ref) => {
        const navigation = useNavigation();
        const { deleteMeal } = useContext(MealsContext);

        const handleConfirm = () => {
            deleteMeal(targetMeal);

            navigation.navigate('LandingPage');
        };

        return (
            <DDModal.Root ref={ref}>
                <DDModal.Title>{MEAL_MANAGEMENT.WARNINGS.DELETE}</DDModal.Title>

                <DDModal.Footer>
                    <DDFlex.Row justifyContent='space-between'>
                        <DDFlex.Column size={6}>
                            <DDButton.Root
                                isFilled={false}
                                onPress={() => ref?.current.blur()}>
                                <DDButton.Label>
                                    {OPTIONS.CANCEL}
                                </DDButton.Label>
                            </DDButton.Root>
                        </DDFlex.Column>

                        <DDFlex.Column size={6}>
                            <DDButton.Root onPress={handleConfirm}>
                                <DDButton.Label>
                                    {MEAL_MANAGEMENT.PROCEED.DELETE}
                                </DDButton.Label>
                            </DDButton.Root>
                        </DDFlex.Column>
                    </DDFlex.Row>
                </DDModal.Footer>
            </DDModal.Root>
        );
    },
);

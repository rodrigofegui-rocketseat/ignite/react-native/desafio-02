import { MealID } from '@app/types';

export type ConfirmDeleteProps = {
    targetMeal: MealID;
};

import styled from 'styled-components/native';

export const ActionsArea = styled.View`
    width: 100%;
    flex-direction: row;

    justify-content: space-between;
`;

import styled from 'styled-components/native';

export const Container = styled.View`
    width: 100%;

    margin-bottom: ${({ theme }) => theme.SPACING.SM}px;
`;

export const EditButtonWrapper = styled.View`
    margin-bottom: ${({ theme }) => theme.SPACING.XS}px;
`;

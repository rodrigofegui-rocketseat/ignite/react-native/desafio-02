import { useNavigation, useRoute } from '@react-navigation/native';
import { FC, useRef } from 'react';
import { TextInput } from 'react-native';
import { useTheme } from 'styled-components/native';

import { DDButton } from '@app/components';
import { MEAL_MANAGEMENT } from '@app/texts';

import { UseDetailsRouteProp } from '../../types';

import { ConfirmDelete } from './components';
import { Container, EditButtonWrapper } from './styled';

export const Actions: FC = () => {
    const navigation = useNavigation();
    const {
        params: { targetMeal },
    } = useRoute<UseDetailsRouteProp>();
    const { SPACING } = useTheme();
    const fakeConfirmDeleteRef = useRef<TextInput>(null);

    return (
        <Container>
            <EditButtonWrapper>
                <DDButton.Root
                    onPress={() =>
                        navigation.navigate('Manipulation', { targetMeal })
                    }>
                    <DDButton.Icon
                        name='PencilSimpleLine'
                        size={SPACING.SM}
                    />
                    <DDButton.Label>{MEAL_MANAGEMENT.UPDATE}</DDButton.Label>
                </DDButton.Root>
            </EditButtonWrapper>

            <DDButton.Root
                isFilled={false}
                onPress={() => fakeConfirmDeleteRef?.current?.focus()}>
                <DDButton.Icon
                    name='Trash'
                    size={SPACING.SM}
                />
                <DDButton.Label>{MEAL_MANAGEMENT.DELETE}</DDButton.Label>
            </DDButton.Root>

            <ConfirmDelete
                ref={fakeConfirmDeleteRef}
                targetMeal={targetMeal}
            />
        </Container>
    );
};

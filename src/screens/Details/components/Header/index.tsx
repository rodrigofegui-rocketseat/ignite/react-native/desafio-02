import { useRoute } from '@react-navigation/native';
import { FC, useContext } from 'react';

import { DDHeader } from '@app/components';
import { MealsContext } from '@app/contexts/MealsContext';
import { MEAL_MANAGEMENT } from '@app/texts';
import { getTheme } from '@app/utils/diet';

import { UseDetailsRouteProp } from '../../types';

export const Header: FC = () => {
    const {
        params: { targetMeal },
    } = useRoute<UseDetailsRouteProp>();
    const { getMeal } = useContext(MealsContext);

    const meal = getMeal(targetMeal);

    return (
        <DDHeader.Root
            color={getTheme(undefined, meal?.inDiet)}
            backTo='LandingPage'>
            <DDHeader.Title>{MEAL_MANAGEMENT.DETAILS}</DDHeader.Title>
        </DDHeader.Root>
    );
};

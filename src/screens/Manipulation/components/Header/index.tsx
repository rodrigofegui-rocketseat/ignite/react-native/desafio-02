import { useRoute } from '@react-navigation/native';
import { FC } from 'react';

import { DDHeader } from '@app/components';
import { MEAL_MANAGEMENT } from '@app/texts';

import { UseManipulationRouteProp } from '../../types';

export const Header: FC = () => {
    const { params } = useRoute<UseManipulationRouteProp>();

    return (
        <DDHeader.Root color='LIGHT'>
            <DDHeader.Title>
                {params?.targetMeal
                    ? MEAL_MANAGEMENT.UPDATE
                    : MEAL_MANAGEMENT.ADD}
            </DDHeader.Title>
        </DDHeader.Root>
    );
};

import { MealID } from '@app/types';

export type PossibleMeal = {
    id?: MealID | null;
    registeredAt?: Moment;
    name?: string | null;
    description?: string | null;
    inDiet?: boolean | null;
};

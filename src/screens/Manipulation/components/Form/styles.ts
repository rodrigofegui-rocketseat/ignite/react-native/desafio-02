import styled from 'styled-components/native';

import { DDContentView, DDFlex } from '@app/components';

export const Container = styled(DDContentView)`
    align-items: center;
    flex: 1;
`;

export const Row = styled(DDFlex.Row).attrs(() => ({
    marginBottom: 'MD',
}))``;

export const ActionArea = styled.View`
    flex: 1;
    width: 100%;
    justify-content: flex-end;

    padding-bottom: ${({ theme }) => theme.SPACING.MD}px;
`;

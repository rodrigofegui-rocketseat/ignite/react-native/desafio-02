import { useNavigation, useRoute } from '@react-navigation/native';
import { FC, useContext, useRef } from 'react';
import { TextInput } from 'react-native';

import { DDButton, DDFlex, DDInput } from '@app/components';
import { MealsContext } from '@app/contexts/MealsContext';
import { MEAL_MANAGEMENT, OPTIONS } from '@app/texts';
import { MAX_MEAL_DESCRIPTION_SIZE, MAX_MEAL_NAME_SIZE } from '@app/types';
import { handleError } from '@app/utils/errors';

import { UseManipulationRouteProp } from '../../types';

import { ActionArea, Container, Row } from './styles';
import { getFilledMeal, showAlert } from './utils/behavior';

export const Form: FC = () => {
    const { addMeal, getMeal, updateMeal } = useContext(MealsContext);
    const navigation = useNavigation();
    const { params } = useRoute<UseManipulationRouteProp>();

    const meal = getMeal(params?.targetMeal);

    const nameRef = useRef<TextInput>(null);
    const descriptionRef = useRef<TextInput>(null);
    const dateRef = useRef<TextInput>(null);
    const timeRef = useRef<TextInput>(null);
    const inDietRef = useRef<boolean>(null);

    console.debug('Manipulation form targetMeal', params?.targetMeal);

    const handleFinish = () => {
        handleError({
            underGuard: () => {
                const possibleMeal = getFilledMeal(
                    nameRef,
                    descriptionRef,
                    dateRef,
                    timeRef,
                    inDietRef,
                    meal,
                );

                const mealHandler = params?.targetMeal ? updateMeal : addMeal;

                mealHandler(possibleMeal);

                navigation.navigate('FeedbackCompleteManipulation', {
                    inDiet: possibleMeal.inDiet,
                });
            },
            onErrorMessage: (message: string) => showAlert(message, meal),
        });
    };

    return (
        <Container>
            <Row>
                <DDInput.Root>
                    <DDInput.Label>{MEAL_MANAGEMENT.ASK.NAME}</DDInput.Label>
                    <DDInput.TextInput
                        ref={nameRef}
                        maxLength={MAX_MEAL_NAME_SIZE}
                        initialValue={meal?.name}
                    />
                </DDInput.Root>
            </Row>

            <Row>
                <DDInput.Root>
                    <DDInput.Label>
                        {MEAL_MANAGEMENT.ASK.DESCRIPTION}
                    </DDInput.Label>
                    <DDInput.TextInput
                        ref={descriptionRef}
                        numberOfLines={5}
                        maxLength={MAX_MEAL_DESCRIPTION_SIZE}
                        initialValue={meal?.description}
                    />
                </DDInput.Root>
            </Row>

            <Row justifyContent='space-between'>
                <DDFlex.Column size={6}>
                    <DDInput.Root>
                        <DDInput.Label>
                            {MEAL_MANAGEMENT.ASK.DATE}
                        </DDInput.Label>
                        <DDInput.DateTimeInput
                            ref={dateRef}
                            mode='date'
                            initialValue={meal?.registeredAt}
                        />
                    </DDInput.Root>
                </DDFlex.Column>

                <DDFlex.Column size={6}>
                    <DDInput.Root>
                        <DDInput.Label>
                            {MEAL_MANAGEMENT.ASK.TIME}
                        </DDInput.Label>
                        <DDInput.DateTimeInput
                            ref={timeRef}
                            mode='time'
                            initialValue={meal?.registeredAt}
                        />
                    </DDInput.Root>
                </DDFlex.Column>
            </Row>

            <Row>
                <DDInput.Root>
                    <DDInput.Label>{MEAL_MANAGEMENT.ASK.IN_DIET}</DDInput.Label>
                    <DDInput.RadioButtonInput.Group
                        ref={inDietRef}
                        initialValue={meal?.inDiet}>
                        <DDInput.RadioButtonInput.Item
                            identifier='primary'
                            content={OPTIONS.YES}
                            value
                        />
                        <DDInput.RadioButtonInput.Item
                            identifier='secondary'
                            content={OPTIONS.NO}
                            value={false}
                        />
                    </DDInput.RadioButtonInput.Group>
                </DDInput.Root>
            </Row>

            <ActionArea>
                <DDButton.Root onPress={handleFinish}>
                    <DDButton.Label>
                        {params?.targetMeal
                            ? MEAL_MANAGEMENT.PROCEED.UPDATE
                            : MEAL_MANAGEMENT.PROCEED.ADD}
                    </DDButton.Label>
                </DDButton.Root>
            </ActionArea>
        </Container>
    );
};

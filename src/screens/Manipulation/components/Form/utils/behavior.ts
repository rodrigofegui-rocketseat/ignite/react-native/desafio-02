import { RefObject } from 'react';
import { Alert, TextInput } from 'react-native';

import { MissingFieldsError } from '@app/errors';
import { MEAL_MANAGEMENT } from '@app/texts';
import { Meal } from '@app/types';

import { PossibleMeal } from '../types';

import { concatDatetime } from './data';

export const focusOnNext = (nextRef: RefObject<TextInput>, meal?: Meal) => {
    if (meal) return;

    nextRef?.current?.focus();
};

export const getFilledMeal = (
    nameRef: RefObject<TextInput>,
    descriptionRef: RefObject<TextInput>,
    dateRef: RefObject<TextInput>,
    timeRef: RefObject<TextInput>,
    inDietRef: RefObject<boolean>,
    initialMeal?: Meal,
): Meal => {
    const possibleMeal: PossibleMeal = {
        id: initialMeal?.id,
        name: nameRef?.current?.value,
        description: descriptionRef?.current?.value,
        registeredAt: concatDatetime(
            dateRef?.current?.value,
            timeRef?.current?.value,
        ),
        inDiet: inDietRef?.current,
    };

    if (!isMinimalFilled(possibleMeal)) {
        throw new MissingFieldsError();
    }

    return possibleMeal;
};

export const showAlert = (message: string, meal?: Meal) => {
    const title = getAlertTitle(meal);

    Alert.alert(title, message);
};

const getAlertTitle = (meal?: Meal) => {
    return meal ? MEAL_MANAGEMENT.UPDATE : MEAL_MANAGEMENT.ADD;
};

const isMinimalFilled = (meal: PossibleMeal): meal is Meal => {
    return Boolean(
        meal.name &&
            meal.description &&
            meal.registeredAt &&
            meal.inDiet !== null,
    );
};

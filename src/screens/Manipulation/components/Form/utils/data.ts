import moment from 'moment';

import { FORMAT } from '@app/texts';
import { NullableMoment } from '@app/types';

export const concatDatetime = (
    date?: string,
    time?: string,
): NullableMoment => {
    if (!(date && time)) {
        return null;
    }

    return moment(
        `${date} ${time} -0300`,
        `${FORMAT.LOCALE_DATE} ${FORMAT.TIME} Z`,
    );
};

import { FC } from 'react';

import { Form, Header } from './components';
import { Container } from './styles';

export const Manipulation: FC = () => {
    return (
        <Container>
            <Header />

            <Form />
        </Container>
    );
};

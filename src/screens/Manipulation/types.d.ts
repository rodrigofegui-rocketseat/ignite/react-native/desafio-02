import { UseAppRouteProp } from '@app/types';

export type UseManipulationRouteProp = UseAppRouteProp<'Manipulation'>;

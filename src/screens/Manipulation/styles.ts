import styled from 'styled-components/native';

import { DDBaseView } from '@app/components';

export const Container = styled(DDBaseView).attrs(() => ({
    withPadding: false,
}))``;

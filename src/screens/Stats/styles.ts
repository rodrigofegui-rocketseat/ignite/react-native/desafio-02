import styled from 'styled-components/native';

import { DDBaseView } from '@app/components/DDBaseView';

export const Container = styled(DDBaseView).attrs(() => ({
    withPadding: false,
}))``;

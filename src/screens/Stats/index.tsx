import { FC, useContext } from 'react';

import { MealsContext } from '@app/contexts/MealsContext';

import { Header, StatsDisplayer } from './components';
import { Container } from './styles';

export const Stats: FC = () => {
    const { stats } = useContext(MealsContext);

    return (
        <Container>
            <Header stats={stats} />

            <StatsDisplayer stats={stats} />
        </Container>
    );
};

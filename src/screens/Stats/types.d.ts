import { DietStats } from '@app/types';

export type StatsProps = {
    stats: DietStats;
};

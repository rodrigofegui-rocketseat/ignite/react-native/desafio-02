import styled from 'styled-components/native';

import { DDContentView, DDText } from '@app/components';

export const Container = styled(DDContentView)`
    align-items: center;
`;

export const Title = styled(DDText).attrs(() => ({
    bold: true,
}))`
    margin-bottom: ${({ theme }) => theme.SPACING.MD}px;
`;

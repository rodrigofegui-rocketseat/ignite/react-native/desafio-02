import { FC } from 'react';

import { DDFlex, DDInfoCard } from '@app/components';
import { DIET_STATS } from '@app/texts';

import { StatsProps } from '../../types';

import { Container, Title } from './styles';

export const StatsDisplayer: FC<StatsProps> = ({ stats }) => {
    return (
        <Container>
            <Title>{DIET_STATS.TITLE}</Title>

            <DDFlex.Row marginBottom='XS'>
                <DDInfoCard.Root>
                    <DDInfoCard.Title>{stats.inRoll}</DDInfoCard.Title>

                    <DDInfoCard.Description>
                        {DIET_STATS.IN_ROLL}
                    </DDInfoCard.Description>
                </DDInfoCard.Root>
            </DDFlex.Row>

            <DDFlex.Row marginBottom='XS'>
                <DDInfoCard.Root>
                    <DDInfoCard.Title>{stats.total}</DDInfoCard.Title>

                    <DDInfoCard.Description>
                        {DIET_STATS.TOTAL_CNT}
                    </DDInfoCard.Description>
                </DDInfoCard.Root>
            </DDFlex.Row>

            <DDFlex.Row
                marginBottom='XS'
                justifyContent='space-between'>
                <DDFlex.Column size={6}>
                    <DDInfoCard.Root color='PRIMARY'>
                        <DDInfoCard.Title>{stats.inDiet}</DDInfoCard.Title>

                        <DDInfoCard.Description>
                            {DIET_STATS.IN_DIET}
                        </DDInfoCard.Description>
                    </DDInfoCard.Root>
                </DDFlex.Column>

                <DDFlex.Column size={6}>
                    <DDInfoCard.Root color='SECONDARY'>
                        <DDInfoCard.Title>{stats.outDiet}</DDInfoCard.Title>

                        <DDInfoCard.Description>
                            {DIET_STATS.OUT_DIET}
                        </DDInfoCard.Description>
                    </DDInfoCard.Root>
                </DDFlex.Column>
            </DDFlex.Row>
        </Container>
    );
};

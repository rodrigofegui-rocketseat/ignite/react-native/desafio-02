import { FC } from 'react';

import { DDHeader } from '@app/components';
import { DIET_STATS } from '@app/texts';
import { getTheme } from '@app/utils/diet';
import { fmtPercent } from '@app/utils/number';

import { StatsProps } from '../../types';

export const Header: FC<StatsProps> = ({
    stats: { adherencePercent, inDietOnZeroAdherence },
}) => {
    return (
        <DDHeader.Root
            color={getTheme(adherencePercent, inDietOnZeroAdherence)}
            backTo='LandingPage'>
            <DDHeader.Title>{fmtPercent(adherencePercent)}</DDHeader.Title>
            <DDHeader.Subtitle>{DIET_STATS.SUMMARY}</DDHeader.Subtitle>
        </DDHeader.Root>
    );
};

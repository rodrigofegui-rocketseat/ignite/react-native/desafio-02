import {
    NunitoSans_400Regular,
    NunitoSans_700Bold,
    useFonts,
} from '@expo-google-fonts/nunito-sans';
import * as SplashScreen from 'expo-splash-screen';
import { FC, useCallback } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { ThemeProvider } from 'styled-components/native';

import { Routes } from '@app/routes';
import { MainTheme } from '@app/themes';

SplashScreen.preventAutoHideAsync();

const App: FC = () => {
    const [isFontsLoaded] = useFonts({
        NunitoSans_400Regular,
        NunitoSans_700Bold,
    });

    const hideSplashscreenWhenFonts = useCallback(async () => {
        if (isFontsLoaded) {
            await SplashScreen.hideAsync();
        }
    }, [isFontsLoaded]);

    if (!isFontsLoaded) {
        return null;
    }

    return (
        <SafeAreaProvider onLayout={hideSplashscreenWhenFonts}>
            <ThemeProvider theme={MainTheme}>
                <Routes />
            </ThemeProvider>
        </SafeAreaProvider>
    );
};

export default App;

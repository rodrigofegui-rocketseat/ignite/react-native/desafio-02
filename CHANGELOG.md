<!-- markdownlint-disable MD024 -->
# Changelog

> Como lembrete: [Changelog vs. Release Notes: Differences and Examples](https://blog.releasenotes.io/changelog-vs-release-notes/)

Todas as distintas mudanças que ocorrem com este projeto serão registradas neste arquivo.

O formato está baseado em [Mantenha um Changelog](https://keepachangelog.com/pt-BR/1.0.0/),
e este projeto segue o [Versionamento Semântico 2.0.0](https://semver.org/lang/pt-BR/).

## [Exemplo] - YYYY-MM-DD

> [Comparação completa]

### Adicionado

para novos recursos.

### Alterado

para alterações na funcionalidade existente.

### Depreciado

para recursos que serão removidos em breve.

### Removido

por recursos removidos.

### Consertado

para qualquer correção de bug.

### Segurança

em caso de vulnerabilidades.

## [Em andamento]

## [v1.0.0](https://gitlab.com/rodrigofegui-rocketseat/ignite/react-native/desafio-02/-/tags/v1.0.0) - 2024-02-07

### Adicionado

- Renderização da tela inicial da aplicação
- Renderização da tela de estatísticas da dieta
- Renderização da tela de gerenciamento de refeição
- Renderização da tela de feedback de manipulação de refeição
- Renderização da tela de detalhamento de refeição
- Navegabilidade entre telas
- Inclusão de refeição no histórico
- Detalhamento de refeição
- Exclusão de refeição do histórico
- Atualização de refeição no histórico
- Estatísticas do histórico
